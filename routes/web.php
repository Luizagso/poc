<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Usuario
Auth::routes();
Route::resource('usuario','UserController');
Route::get('usuario/teste','HomeController@teste')->name('usuario.teste');
//Route::resource('login','LoginController');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/','HomeController@index')->name('index');

//Instituicao
Route::resource('instituicao','InstituicaoController');
Route::get('datatable/instituicao', 'InstituicaoController@dataTableJson')->name('instituicao.datatable');

//Ativos
Route::resource('ativos','AtivosController');
Route::get('datatable/ativos', 'AtivosController@dataTableJson')->name('ativos.datatable');

//Endereços
Route::resource('enderecos','EnderecosController');

//Estados
Route::resource('estados','EstadosController');
Route::get('estados/{estado}','EstadosController@getCidades')->name('estado.cidade');

//Dividendos
Route::resource('dividendos','DividendosController');

//Produtos
Route::resource('produtos','ProdutosController');
Route::get('datatable/produtos', 'ProdutosController@dataTableJson')->name('produtos.datatable');

//IMPOSTO
Route::resource('imposto','ImpostoController');

//PDF PRODUTOS
Route::get('pdf/produtos','ProdutosController@gerarPDF')->name('pdf.produtos');

//PDF DIVIDENDOS
Route::get('pdf/dividendos','DividendosController@gerarPDF')->name('pdf.dividendos');

//PDF RENDIMENTOS
Route::get('pdf/rendimentos','HomeController@gerarPDF')->name('pdf.rendimentos');