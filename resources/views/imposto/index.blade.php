@extends('layouts.master')
@section('body')
<div class="row border-bottom border-secondary rounded-bottom p-3 shadow-sm">
    <img src="{{ asset('img/barras.png')}}" alt="Ações" height="40px" width="40px" class="mr-3">
    <h2 class="text-secondary">Cálculo de ações no IR</h2>
</div>
<br>


<div class="shadow-sm p-3 mt-3 ml-1 bg-white rounded w-100 text-secondary row">

    <label for="" class="mt-1">Selecione o ano que você deseja: </label>
    <button id="bt1" class="ml-3 btn text-secondary" style="background-color: #c6ecc6;">2018</button>
    <button id="bt2" class="ml-3 btn text-secondary" style="background-color: #c6ecc6;">2019</button>
    <button id="bt3" class="ml-3 btn text-secondary" style="background-color: #c6ecc6;">2020</button>

</div>
<br>


<div id="primeiro" class="text-secondary" >
    <h4 class="ml-3" >ANO 2018</h4>
    <?php 
        $tamanho= count($com1);
    ?>
    @if($tamanho>0)
        @foreach ($com1 as $compras)

            <div class="shadow-sm p-3 mt-3 ml-1 bg-white rounded w-100 text-secondary row ">
                <div class="float-right w-50">

                    <h4>Ação: {{$compras->atiSigla}}</h4> 
                    <label for="" class="mt-2"> Total bruto de compras: R$ {{ round($compras->Total * $compras->Preco, 2)}}</label>
                    <br>
                    @foreach($div1 as $div)
                        @if($div->atiSigla == $compras->atiSigla)
                            <label for="">Total bruto de dividendos: R$ {{round($div->divValor * $div->Total, 2)}}</label> <br>   
                        @endif
                    @endforeach

                    @foreach($jscp1 as $jscp)
                        @if($jscp->atiSigla == $compras->atiSigla)
                            <label for="">Total bruto de JSCP: R$ {{round($jscp->divValor * $jscp->Total, 2)}}</label> <br>
                        @endif
                    @endforeach

                    @foreach($ven1 as $ven)

                        @if($ven->atiSigla == $compras->atiSigla)
                            <label for="">Total bruto de vendas: R$ {{ round($ven->Total *$ven->Preco, 2)}}</label> <br>
                </div>
                <div class="float-left w-50 mt-5">
                        @if((($ven->Total *$ven->Preco) - ($compras->Total * $compras->Preco)) > 0)
                            @if(($ven->Total *$ven->Preco)>20000)
                                <label for="">Imposto a ser retido: R$ {{ round($ven->Preco * 0.00005 * $ven->Total, 2)}} </label> <br>
                                <label for="" class="text-primary">Tributo a ser declarado (alíquota de 15%): R$ {{round((($ven->Total *$ven->Preco) - ($compras->Total * $compras->Preco)) * 0.15, 2)}} </label> <br>
                            @endif
                            <label for="" class="text-success"> Lucro: R$ {{round(($ven->Total *$ven->Preco) - ($compras->Total * $compras->Preco), 2)}} </label> <br>
                        @else
                            <label for="" class="text-danger">Prejuízo: {{ round(($ven->Total *$ven->Preco) - ($compras->Total * $compras->Preco), 2)}} </label>
                            <br>
                            @endif
                        @endif
                    @endforeach
                </div>
            </div>
        @endforeach
    @else
    <div class="shadow-sm p-3 mt-3 ml-1 bg-white rounded w-100 text-danger ">
        <label for="">Não há registros de compra ou venda de ações nesse ano.</label>
    </div> 
    @endif
</div>

<div id="segundo" class="text-secondary" >
    <h4 class="ml-3" >ANO 2019</h4>

    <?php 
        $tamanho2= count($com2);
    ?>
    @if($tamanho2>0)
        
        @foreach ($com2 as $compras)

            <div class="shadow-sm p-3 mt-3 ml-1 bg-white rounded w-100 text-secondary row ">

                <div class="float-right w-50">

                    <h4>Ação: {{$compras->atiSigla}}</h4> 
                    <?php
                        $pm=0;
                        foreach($PMC2 as $prod){
                            if($prod->atiSigla == $compras->atiSigla){
                                $preco= 0;
                                $pm=0;
                                foreach($PMC2 as $p){
                                    if($prod->atiSigla == $p->atiSigla){
                                        $preco= $pm + ($p->proPreco *$p->proQuantidade);
                                    }
                                    $pm= $preco;
                                }
                            }
                        }
                        echo('<label for="" class="mt-2"> Total bruto de compras: R$'.$pm.'</label>');
                    ?>
                    <br>

                    @foreach($div2 as $div)
                        @if($div->atiSigla == $compras->atiSigla)
                            <label for="">Total bruto de dividendos: R$ {{round($div->divValor * $div->Total, 2)}}</label> <br>   
                        @endif
                    @endforeach

                    @foreach($jscp2 as $jscp)
                        @if($jscp->atiSigla == $compras->atiSigla)
                            <label for="">Total bruto de JSCP: R$ {{round($jscp->divValor * $jscp->Total, 2)}}</label> <br>
                            <label for="" class="text-primary">Tributo sobre JSCP (15%): R$ {{round(($jscp->divValor * $jscp->Total) * 0.15, 2)}}</label> <br>
                        @endif
                    @endforeach

                    @foreach($ven2 as $ven)

                        @if($ven->atiSigla == $compras->atiSigla)

                        <?php
                            $pm=0;
                            foreach($PMV2 as $prod){
                                if($prod->atiSigla == $compras->atiSigla){
                                    $preco= 0;
                                    $pm=0;
                                    foreach($PMV2 as $p){
                                        if($prod->atiSigla == $p->atiSigla){
                                            $preco= $pm + ($p->proPreco *$p->proQuantidade);
                                        }
                                        $pm= $preco;
                                    }
                                }
                            }
                            echo('<label for="" class="mt-2"> Total bruto de vendas: R$'.$pm.'</label>');
                        ?>
                        <br>
                </div>
                <div class="float-left w-50 mt-5">
                        @if((($ven->Total *$ven->Preco) - ($compras->Total * $compras->Preco)) > 0)
                            <label for="" class="text-success"> Lucro: R$ {{round(($ven->Total *$ven->Preco) - ($compras->Total * $compras->Preco), 2)}} </label> <br>
                            @if(($ven->Total *$ven->Preco)>20000)
                                <label for="">Imposto a ser retido: R$ {{ round($ven->Preco * 0.00005 * $ven->Total, 2)}} </label> <br>
                                <label for="" class="text-primary">Tributo a ser declarado (alíquota de 15%): R$ {{round((($ven->Total *$ven->Preco) - ($compras->Total * $compras->Preco)) * 0.15, 2)}} </label> <br>
                            @endif
                            
                        @else
                            <label for="" class="text-danger">Prejuízo: {{ round(($ven->Total *$ven->Preco) - ($compras->Total * $compras->Preco), 2)}} </label>
                            <br>
                        @endif
                    @endif
                @endforeach
                </div>
            </div>
        @endforeach
    @else
        <div class="shadow-sm p-3 mt-3 ml-1 bg-white rounded w-100 text-danger ">
            <label for="">Não há registros de compra ou venda de ações nesse ano.</label>
        </div> 
    @endif
</div>

<div id="terceiro" class="text-secondary" >
    <h4 class="ml-3" >ANO 2020</h4>

    <?php 
        $tamanho3= count($com3);
    ?>

    @if($tamanho3>0)
        @foreach ($com3 as $compras)

            <div class="shadow-sm p-3 mt-3 ml-1 bg-white rounded w-100 text-secondary row ">

                <div class="float-right w-50">

                    <h4>Ação: {{$compras->atiSigla}}</h4>
                    
                    <label for="" class="mt-2"> Total bruto de compras: R$ {{ round($compras->Total * $compras->Preco, 2)}}</label>
                    <br>
                    @foreach($div3 as $div)
                        @if($div->atiSigla == $compras->atiSigla)
                            <label for="">Total bruto de dividendos: R$ {{round($div->divValor * $div->Total, 2)}}</label> <br>   
                        @endif
                    @endforeach

                    @foreach($jscp3 as $jscp)
                        @if($jscp->atiSigla == $compras->atiSigla)
                            <label for="">Total bruto de JSCP: R$ {{round($jscp->divValor * $jscp->Total, 2)}}</label> <br>
                        @endif
                    @endforeach

                    @foreach($ven3 as $ven)

                        @if($ven->atiSigla == $compras->atiSigla)
                            <label for="">Total bruto de vendas: R$ {{ round($ven->Total *$ven->Preco, 2)}}</label> <br>
                </div>
                <div class="float-left w-50 mt-5">
                        @if((($ven->Total *$ven->Preco) - ($compras->Total * $compras->Preco)) > 0)
                            @if(($ven->Total *$ven->Preco)>20000)
                                <label for="">Imposto a ser retido: R$ {{ round($ven->Preco * 0.00005 * $ven->Total, 2)}} </label> <br>
                                <label for="" class="text-primary">Tributo a ser declarado (alíquota de 15%): R$ {{round((($ven->Total *$ven->Preco) - ($compras->Total * $compras->Preco)) * 0.15, 2)}} </label> <br>
                            @endif
                            <label for="" class="text-success"> Lucro: R$ {{round(($ven->Total *$ven->Preco) - ($compras->Total * $compras->Preco), 2)}} </label> <br>
                        @else
                            <label for="" class="text-danger">Prejuízo: {{ round(($ven->Total *$ven->Preco) - ($compras->Total * $compras->Preco), 2)}} </label>
                            <br>
                        @endif
                    @endif
                @endforeach
                </div>
            </div>
        @endforeach
    @else
        <div class="shadow-sm p-3 mt-3 ml-1 bg-white rounded w-100 text-danger ">
            <label for="">Não há registros de compra ou venda de ações nesse ano.</label>
        </div> 
    @endif
</div>


@endSection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

<script type= "text/javascript">

    $(function(){
        $('#primeiro').hide();
        $('#segundo').hide();
        $('#terceiro').hide();

        $('#bt1').click(function(){
            $('#primeiro').show();
            $('#segundo').hide();
            $('#terceiro').hide();
        });

        $('#bt2').click(function(){
           $('#primeiro').hide();
            $('#segundo').show();
            $('#terceiro').hide();
        });

        $('#bt3').click(function(){
            $('#primeiro').hide();
            $('#segundo').hide();
            $('#terceiro').show();
        });
    });

</script>