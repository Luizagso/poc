@extends('layouts.master')

@section('body')

<!--Mensagem success-->
@if(Session::has('success'))
	<div class="alert alert-success alert-dimissible fade show">
		{{ Session::get('success') }}
		<button type="button" class="close" data-dismiss='alert' arial-label="Close">
			<span aria-hidden = "true"> &times; </span>
		</button>
	</div>
@endIF

<!--CABEÇALHO-->
<div class="row border-bottom border-secondary rounded-bottom p-3 shadow-sm">
    <img src="{{ asset('img/barras.png')}}" alt="Ações" height="40px" width="40px" class="mr-3">
    <h2 class="text-secondary">Dividendos e JSCP</h2>
    <a class="ml-auto mr-2" href="{{ route('dividendos.create') }}">
        <button class="btn btn-success">
            Cadastrar
        </button>
    </a>
    <a class="mr-2" href="{{ route('pdf.dividendos') }}">
        <button class="btn btn-dark text-light">
            PDF
        </button>
    </a>
</div>

<br><br>

<!--TABELA-->
<div class="">
        <table class="table table-dark text-dark" id="produtos-table">
            <thead>
                <tr>
                    <th class="text-light">Ação</th>
                    <th class="text-light">Tipo</th>
                    <th class="text-light">Valor por ação </th>
                    <th class="text-light">Valor </th>
                    <th class="text-light">Data </th>
                    <th class="text-light">Opções </th>
                </tr>
                <tbody>
                    @foreach ($produtos as $produtos)
                        <tr>
                            <td>{{$produtos->atiSigla}}</td>
                            <td>{{$produtos->divTipo}}</td>
                            <td>R${{$produtos->divValor}}</td>
                            <td>R${{$produtos->divValor * $produtos->Total}}</td>
                            <td> <input  class="border-0 bg-white text-dark" type="date" disabled="disabled" value="{{$produtos->divData}}"></td>
                            <td>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <a href="{{ route('dividendos.edit', $produtos->divCodigo) }}" class="btn btn-primary" style="height:40px;">
                                            <i class="fa fa-edit mr-1"></i>
                                            Editar
                                        </a>
                                    </div>
                                    <div class="col-lg-6">
                                        <button type="button" class="btn btn-danger" style="height:40px;" data-toggle="modal" data-target="#modalExcluir" onclick="abrirModal('{{$produtos->divCodigo}}')"> 
                                            <i class="fa fa-trash-o mr-2"></i>
                                            Apagar
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </thead>
        </table>
</div>


<!--MODAL-->
<div class="modal fade" tabindex="-1" role="dialog" id="modalExcluir">
        <div class="modal-dialog border rounded" role="document">
            <div class="modal-content border rounded">
                <div class="modal-header bg-danger" style="height:50px;">
                    <h5 class="modal-title text-light align-center">ALERTA!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <input type="hidden" id="codigo">
                <div class="modal-body bg-light">
                        <span>Deseja</span>
                        <span class="text-danger">excluir</span>
                        <span>o item selecionado?</span>
                        <p id="modalBody"> </p>
                </div>
                <div class="modal-footer bg-light">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-danger" onclick="excluir()">Excluir</button>
                </div>
            </div>
        </div>
</div>

@endSection


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script type="text/javascript">

    jQuery(function() {
                jQuery('#produtos-table').DataTable({ 
                    dom: 'lBfrtip',
                    "language": {
                        "url": "http://cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
                    },
                    buttons: [
                        'pdfHtml5',
                        'excelHtml5'
                    ]
                });
    });

    function abrirModal(id) {
        var divCodigo= id;
        console.log(divCodigo);
        $.ajax({
            type:'GET',
            url:'dividendos',
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: false,
                error: function(response) {
                    console.log("Erro:\n"+response);
                },
                success: function(resposta) {
                    if(resposta != 'Vazio'){
                        codigo.value = divCodigo;
                        //modalBody.innerHTML = "Deseja excluir o item selecionado?";
                        $('#modalExcluir').modal('show');
                    } else {
                        alert('Não é possível realizar a exclusão');
                    }
                }
        });
    }

    function excluir(){        
        var divCodigo = codigo.value;
        console.log(divCodigo);

        $.ajax({
            type:'DELETE',
            url: 'dividendos/'+divCodigo,
            //data: "{}",
            //contentType: "application/json; charset=utf-8",
            //dataType: "json",
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Content-type': 'application/json',
                'Authorization': '${this.state.tokenType} ${this.state.token}' 
            },
            cache: false,
                error: function(response) {
                    console.log("Erro:\n"+response);
                },
                success: function(resposta) {
                    if(resposta == true){
                        $('#modalExcluir').modal('hide');
                        console.log("ok");
                        window.location.assign('dividendos');
                    } else {
                        alert('Não é possível realizar a exclusão');
                    }
                }
        }); 
    }

  </script>
