<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PDF</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">  
    <link href="{{ asset('img/logoMini.png') }}" rel="icon">

</head>
<body>

    <div class="w-100" >
        <img src="{{ asset('img/LOGO.png')}}" alt="Logo" height="60px" width="150px" style="margin-left:270px">
        <br>
        <h5 style="margin-left:200px" class="mt-4">Relatório de Dividendos e JSPC</h5>
    </div>
    <br>
    <div class="p-2">
        <label for="" class="ml-2">Seus dividendos e JSPC:</label>
        <br>
        <table class="table ml-3 mr-3 border">
        <thead>
                    <tr>
                        <th class="border" style="font-size: 13px">Ação</th>
                        <th class="border" style="font-size: 13px">Tipo</th>
                        <th class="border" style="font-size: 13px">Valor por ação </th>
                        <th class="border" style="font-size: 13px">Valor </th>
                        <th class="border" style="font-size: 13px">Data </th>
                    </tr>
                    <tbody>
                        @foreach ($produtos as $produtos)
                            <tr>
                                <td class="border" style="font-size: 13px">{{$produtos->atiSigla}}</td>
                                <td class="border" style="font-size: 13px">{{$produtos->divTipo}}</td>
                                <td class="border" style="font-size: 13px">R${{$produtos->divValor}}</td>
                                <td class="border" style="font-size: 13px">R${{$produtos->divValor * $produtos->Total}}</td>
                                <td class="border" style="font-size: 13px">{{$produtos->divData}}</td>
                            </tr>
                        @endforeach
                    </tbody>
            </thead>
        </table>
        <label for="" class="ml-2">Valor total: R${{$Total}} </label>
    
    </div>
    <br><br>

    <div class="ml-3 mr-3">
        <label for="" style="margin-left:280px">{{$data}}</label>
    </div>
</body>
</html>