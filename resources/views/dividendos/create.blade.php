@extends('layouts.master')

@section('body')

<div class="row border-bottom border-secondary rounded-bottom p-3 shadow-sm">
    <img src="{{ asset('img/barras.png')}}" alt="Ações" height="40px" width="40px" class="mr-3">
    <h2 class="text-secondary">Cadastrar Novo Dividendo ou JSCP</h2>
</div>
    <br><br>

<div>
    <form name="formPro" id="formPro" method="POST" action="{{url('dividendos')}}"> <!--Manda pro store do controller-->
        @csrf
        <div class="row"> 
            <div class="w-25 ml-5">
                <div class="form-group ">
                    <label class="text-secondary required">Ativo:</label>
                    <select class="form-control border rounded" name="ativos_atiCodigo" id="ativos_atiCodigo" required>
                        <Option value="">Selecione o ativo</Option>
                        @foreach($produtos as $produtos)
                            <option value="{{ $produtos->ativos->atiCodigo}}" >{{ $produtos->ativos->atiSigla }} - {{$produtos->ativos->atiNome}}</option>    
                        @endforeach
                    </select>
                    @if($errors->get('ativos_atiCodigo'))
                        @foreach($errors->get('ativos_atiCodigo') as $error)
                            <span class="text-danger">{{ $error }}</span>
                        @endForeach
                    @endIF
                </div>
                <div class="form-group">
                    <label class="text-secondary required">Data:</label>
                    <input type="date" class="form-control border rounded" id="divData" name = "divData" placeholder="Insira a data??" required>
                    @if($errors->get('divData'))
                        @foreach($errors->get('divData') as $error)
                            <span class="text-danger">{{ $error }}</span>
                        @endForeach
                    @endIF
                </div>
            </div>
            <div class="w-25 ml-5">
                <div class="form-group">
                    <label class="text-secondary required">Valor por ação:</label>
                    <input type="text" class="form-control border rounded" id="divValor" name = "divValor" placeholder="Insira o valor por ação" required>
                    @if($errors->get('divValor'))
                        @foreach($errors->get('divValor') as $error)
                            <span class="text-danger">{{ $error }}</span>
                        @endForeach
                    @endIF
                </div>
                <div class="form-group">
                    <label class="text-secondary required">Tipo:</label>
                    <select class="form-control border rounded" name="divTipo" id="divTipo" required>
                        <Option value="">Selecione o tipo</Option>
                        <option value="Dividendo">Dividendo</option>
                        <option value="JSCP">JSCP</option>
                    </select>
                    @if($errors->get('divTipo'))
                        @foreach($errors->get('divTipo') as $error)
                            <span class="text-danger">{{ $error }}</span>
                        @endForeach
                    @endIF
                </div>
                <!--<input type="hidden"  name="users_id" id="users_id" value="{{ Auth::user()->id }}">-->
            </div>
            <div class="w-25 ml-5">
                <div class="mt-4">
                        <button type="submit" class="btn btn-success mt-auto w-100">Cadastrar</button>
                        <br><br><br>
                        <a href="{{ route('dividendos.index') }}" class="btn btn-dark w-100">
                            Cancelar
                        </a>
                </div>
            </div>
        </div>

        <div class="ml-5">
            <br>
            <label for="" class="required text-danger"> Campos Obrigatórios: </label>
        </div>
    </form>
</div>

@endSection

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>


<script type="text/javascript">
    jQuery(function($){
            $("#divValor").mask('##0.00', {reverse: true});
    });
</script>