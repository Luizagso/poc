@extends('layouts.master')

@section('body')

    <div class="row border-bottom border-secondary rounded-bottom p-3 shadow-sm">
        <h2 class="text-secondary">Sua Conta</h2>
        <img src="{{ asset('img/conta.png')}}" alt="Moeda" height="40px" width="60px" class="ml-auto mr-1">
    </div>
    <br><br>

    <div>
    
        <form method="POST" action="{{ route('usuario.edit', Auth::user()->id) }}">
        @method('GET')
        @csrf
            <div class="row">
                
                <div class="bg-light border-left rounded w-50">
                    <div class="p-2 border-0 rounded bg-dark text-light shadow-sm">
                        <label for="">Seus dados:</label>
                    </div>
                    <br>
                    <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                        <label for="" class="text-secondary">Nome:</label>
                        <input class="form-control border-0 rounded" value="{{ Auth::user()->name }}" disabled="disabled">
                    </div>
                    <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                        <label for="" class="text-secondary">Email:</label>
                        <input class="form-control border-0 rounded" value="{{ Auth::user()->email }}" disabled="disabled">
                    </div>
                    <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                        <label for="" class="text-secondary">CPF:</label>
                        <input class="form-control border-0 rounded" value="{{ Auth::user()->cpf }}" disabled="disabled">
                    </div>
                    <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                        <label for="" class="text-secondary">Celular:</label>
                        <input class="form-control border-0 rounded" value="{{ Auth::user()->cel }}" disabled="disabled">
                    </div>
                    <!--<div class="p-2 border-bottom" style="border-bottom-color:#778899">
                        <label for="" class="text-secondary">Senha:</label>
                        <input class="form-control border-0 rounded" value="{{ Auth::user()->password }}" type="password" disabled="disabled">
                    </div>-->
                </div>

                <div class="bg-light border-left rounded w-50">
                    <div class="p-2 border-0 rounded bg-dark text-light shadow-sm">
                        <label for="">Seu endereço:</label>
                    </div>
                    <br>
                    @if($enderecos->cidades_cidCodigo==NULL)

                    @else
                        <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                            <label for="" class="text-secondary">Estado:</label>
                            <input class="form-control border-0 rounded" value="{{$enderecos->cidades->estados->estNome}}" disabled="disabled">
                        </div>
                        <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                            <label for="" class="text-secondary">Cidade:</label>
                            <input class="form-control border-0 rounded" value="{{$enderecos->cidades->cidNome}}" disabled="disabled">
                        </div>
                    @endif

                    @if($enderecos->endBairro==NULL)

                    @else
                        <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                            <label for="" class="text-secondary">Bairro:</label>
                            <input class="form-control border-0 rounded" value="{{$enderecos->endBairro}}" disabled="disabled">
                        </div>
                    @endif

                    @if($enderecos->endRua==NULL)

                    @else
                        <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                            <label for="" class="text-secondary">Rua:</label>
                            <input class="form-control border-0 rounded" value="{{$enderecos->endRua}}" disabled="disabled">
                        </div>
                    @endif

                    @if($enderecos->endNumero==NULL)

                    @else
                        <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                            <label for="" class="text-secondary">Número:</label>
                            <input class="form-control border-0 rounded" value="{{$enderecos->endNumero}}" disabled="disabled">
                        </div>
                    @endif

                    @if($enderecos->endComplemento==NULL)
                        
                        @else
                        <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                            <label for="" class="text-secondary">Complemento:</label>
                            <input class="form-control border-0 rounded" value="{{$enderecos->endComplemento}}" disabled="disabled">
                        </div>
                    @endif
                 </div>
            </div>
            <br>
            <div class="row">
                <button class="btn btn-primary ml-auto" type="submit"> 
                    <i class="fa fa-edit mr-1"></i> Editar Dados
                </button>
                <button class="btn btn-danger ml-2 mr-5" data-toggle="modal" data-target="#modalExcluir" type="button">
                    <i class="fa fa-trash-o mr-1"></i> Excluir Conta
                </button>
            </div>
        </form>

            
    </div>

    <!--MODAL-->
    <div class="modal fade" tabindex="-1" role="dialog" id="modalExcluir">
        <div class="modal-dialog border rounded" role="document">
            <div class="modal-content border rounded">
                <div class="modal-header bg-danger" style="height:50px;">
                    <h5 class="modal-title text-light align-center">ALERTA!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <input type="hidden" id="codigo">
                <div class="modal-body bg-light">
                        <span>Deseja</span>
                        <span class="text-danger">excluir</span>
                        <span>a sua conta?</span>
                        <p id="modalBody"> </p>
                </div>
                <div class="modal-footer bg-light" style="height:60px;">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                    <form action="{{ route('usuario.destroy', Auth::user()->id )}}" method="POST">
                    @method('DELETE')
                    @csrf
                        <button type="submit" class="btn btn-danger mt-3">Excluir</button>
                    </form>

                        
                </div>
            </div>
        </div>
    </div>
    

@endSection

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script type="text/javascript">

</script>