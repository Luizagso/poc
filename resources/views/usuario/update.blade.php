@extends('layouts.master')

@section('body')

    <div class="row border-bottom border-secondary rounded-bottom p-3 shadow-sm">
        <h2 class="text-secondary">Editar Conta</h2>
        <img src="{{ asset('img/conta.png')}}" alt="Moeda" height="40px" width="60px" class="ml-auto mr-1">
    </div>
    <br><br>

    <div>
    
        <form method="POST" action="{{ route('usuario.update', $user->id) }}">
        @method('PUT')
        @csrf
            <div class="row">
                <div class="bg-light border-right rounded w-50">

                    <div class="p-2 border-0 rounded bg-dark text-light shadow-sm">
                        <label for="">Edite seus dados:</label>
                    </div>
                    <br>
                    <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                        <label for="name" class="text-secondary">Nome:</label>
                        <input name="name" id="name" class="form-control border-0 rounded" value="{{$user->name}}">
                    </div>
                    <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                        <label for="email" class="text-secondary">Email:</label>
                        <input name="email" id="email"  class="form-control border-0 rounded" value="{{$user->email}}">
                    </div>
                    <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                        <label for="cpf" class="text-secondary">CPF:</label>
                        <input name="cpf" id="cpf"  class="form-control border-0 rounded" value="{{$user->cpf}}">
                    </div>
                    <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                        <label for="cel" class="text-secondary">Celular:</label>
                        <input name="cel" id="cel"  class="form-control border-0 rounded" value="{{$user->cel}}">
                    </div>
                </div>
                <div class="bg-light border-right rounded w-50">
                    <div class="p-2 border-0 rounded bg-dark text-light shadow-sm">
                        <label for="">Edite os dados do seu endereço:</label>
                    </div>
                    <br>
                    <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                        <label for="" class="text-secondary">UF:</label>
                        <select class="form-control dynamic border-0 rounded" id="estados" name="estados" onchange="mostraCidade(this)">
                            @if($user->enderecos->cidades_cidCodigo==NULL)
                                <option value="">Selecione seu Estado</option>
                                @foreach($estados as $estados)
                                    <option value="{{ $estados->estCodigo}}" >{{ $estados->estNome }} - {{ $estados->estUF}} </option>
                                @endforeach
                            @else
                                option class="text-light bg-secondary" value="{{$user->enderecos->cidades->estados->estCodigo}}">
                                    {{$user->enderecos->cidades->estados->estNome}}
                                </option>
                                    
                                @foreach($estados as $estados)
                                    <option value="{{ $estados->estCodigo}}" >{{ $estados->estNome }} - {{ $estados->estUF}} </option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                        <label for="" class="text-secondary">Cidade:</label>
                        <select class="form-control dynamic border-0 rounded" id="cidades_cidCodigo" name="cidades_cidCodigo">
                            @if($user->enderecos->cidades_cidCodigo==NULL)
                                <option value="">Selecione sua cidade</option>
                            @else
                                <option class="text-light bg-secondary" value="{{$user->enderecos->cidades->cidCodigo}}">
                                    {{$user->enderecos->cidades->cidNome}}
                                </option>
                            @endif
                        </select>
                    </div>

                    <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                        <label for="" class="text-secondary">Bairro:</label>
                        <input id="endBairro" name="endBairro" class="form-control border-0 rounded" value="{{$user->enderecos->endBairro}}">
                    </div>
                    <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                        <label for="" class="text-secondary">Rua:</label>
                        <input id="endRua" name="endRua" class="form-control border-0 rounded" value="{{$user->enderecos->endRua}}">
                    </div>
                    <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                        <label for="" class="text-secondary">Número:</label>
                        <input id="endNumero" name="endNumero" class="form-control border-0 rounded" value="{{$user->enderecos->endNumero}}">
                    </div>
                    <div class="p-2 border-bottom" style="border-bottom-color:#778899">
                       <label for="" class="text-secondary">Complemento:</label>
                        <input id="endComplemento" name="endComplemento" class="form-control border-0 rounded" value="{{$user->enderecos->endComplemento}}">
                    </div>
                </div>
            </div>
            <br>

            <input type="hidden" value="{{$user->enderecos_endCodigo}}" name="enderecos_endCodigo" id="enderecos_endCodigo">

            <div class="row">
                <button class="btn btn-success ml-auto mr-2" type="submit">Salvar Alterações</button>
                <a href="{{ route('enderecos.show', $user->enderecos_endCodigo ) }}" class="mr-5">
                  <button class="btn bg-dark text-light" type="button">Cancelar</button>
                </a>
            </div>
        </form>
    

@endSection


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>

<script type="text/javascript">
	var baseurl = window.location.protocol + '//' + window.location.host + '/';

    function mostraCidade(id){
            var estCodigo = id.value;
            console.log(estCodigo);
            $.ajax({
                type: 'GET',
                url:"http://localhost/POC/public/estados/"+estCodigo,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
				},
                cache: false,
                    error: function(response) {
                        console.log("Erro:\n"+response);
                    },
                    success: function(resposta) {
                        console.log(resposta.cidades);
                        $('#cidades_cidCodigo').empty();
                        for(var i=0; i <resposta.tamanho;i++){
                            $('#cidades_cidCodigo').append('<option value=' + resposta.cidades[i].cidCodigo + '>' + resposta.cidades[i].cidNome + '</option>');
                        }
                        
                    }		
                });
    }

    jQuery(function($){
        $("#cel").mask("(99) 99999-9999");
        $("#cpf").mask("999.999.999-99");
    });
</script>