<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PDF</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">  
    <link href="{{ asset('img/logoMini.png') }}" rel="icon">

</head>
<body>

    <div class="w-100" >
        <img src="{{ asset('img/LOGO.png')}}" alt="Logo" height="60px" width="150px" style="margin-left:270px">
        <br>
        <h5 style="margin-left:230px" class="mt-4">Relatório Geral de Investimentos</h5>
    </div>
    <br>
    <div class="p-2">
    
        <label for="" class="ml-2">Ações compradas ou vendidas:</label>
        <br><br>
        <table class="table ml-3 mr-3 border" style="font-size: 13px">
            <thead>
                <tr>
                    <th style="font-size: 13px">Ação</th>
                    <th style="font-size: 13px">Preço Médio</th>
                    <th></th>
                    <th style="font-size: 13px">Quantidade</th>
                    <th style="font-size: 13px">Cotação</th>
                    <th></th>
                    <th style="font-size: 13px ml-3">Valor</th>
                    <th></th>
                    <th style="font-size: 13px ml-3">Lucro/Prejuízo</th>
                </tr>
                <tbody>
                @foreach ($produtos as $produtos)
                    <tr >
                        <td>{{$produtos->atiSigla}}</td>
                        <?php
                            $soma= count($PA);
                            $tamanho= $soma - 1;
                            for($i = 0; $i <= $tamanho;$i++){
                                if($PA[$i] == $produtos->atiSigla){
                                    echo ('<td> R$'.round($PA[$i+1], 2).'<td>');
                                }
                            }
                        ?>
                        <td>{{ $produtos->Total }}</td>
                        <?php

                                       for($i = 0; $i <sizeof($PA);$i++){
                                            if($PA[$i] == $produtos->atiSigla){
                                                $Vpa= $PA[$i+1];
                                                $conf = false;
                                                $soma= count($cotacao2);
                                                $tamanho= $soma - 1;
                                                for($i = 0; $i <= $tamanho;$i++){
                                                    foreach($cotacao2[$i] as $indice => $valor){
                                                        if($conf==true){
                                                            $conf=false;
                                                            $final=0;
                                                            echo ('<td>'.$valor.'<td>');//cotação
                                                            $valor = str_replace(",",".", $valor);
                                                            $valor = (float)$valor;//cotação
                                                            $calculo= $produtos->Total * $valor;//valor
                                                            echo ('<td> R$ '.$calculo.'<td>');
                                                            $calculo2= round(($Vpa*$produtos->Total) - $calculo, 2);
                                                            $calculo= str_replace(".",",", $calculo);
                                                            if($calculo2>0){
                                                                echo ('<td> + R$ '.$calculo2.'<td>');
                                                            }else{
                                                                $calculo2= abs($calculo2);
                                                                echo ('<td> - R$ '.$calculo2.'<td>');
                                                            }
                                                        }
                                                        if($valor == $produtos->atiSigla){
                                                            $conf= true;
                                                        }
                                                    }
                                                }
                                            }
                                       }
                                    ?>
                    </tr>
                @endforeach
                </tbody>
            </thead>
        </table>
    
    </div>
    <br><br>
    <div class="ml-3 mr-3">
        <label for="" style="margin-left:280px">{{$data}}</label>
    </div>
</body>
</html>