
@extends('layouts.master')
@section('body')
    <div class="row border-bottom border-secondary rounded-bottom p-3 shadow-sm">
        <img src="{{ asset('img/barras.png')}}" alt="Ações" height="40px" width="40px" class="mr-3">
        <h2 class="text-secondary">Investimentos</h2>
        <a class="ml-auto mr-2" href="{{ route('pdf.rendimentos') }}">
            Gerar PDF dos seus investimentos
        </a>
    </div>
    <div class="row ">

        @if($produtos->count())
            <div class="shadow-sm p-3 m-2 rounded text-secondary">
                <canvas id="myChart" width="300" height="300"></canvas>
                <?php
                        $nome=array_keys($array);
                        $busca= array();
                        foreach ($nome as $nom) {
                            $busca[] = $nom;
                        }
                ?>
                <script>
                    var ctx = document.getElementById('myChart').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: [ 
                                <?php
                                    foreach ($busca as $bus) {
                                        echo("'".$bus."'");?> ,
                                    <?php 
                                    }
                                ?>
                            ],
                            datasets: [{
                                label: 'Quantidade de Ações',
                                data: [ {{ implode(',', $array)}}],
                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(54, 162, 235, 0.2)',
                                    'rgba(255, 206, 86, 0.2)',
                                    'rgba(75, 192, 192, 0.2)',
                                    'rgba(153, 102, 255, 0.2)',
                                    'rgba(255, 159, 64, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255, 99, 132, 1)',
                                    'rgba(54, 162, 235, 1)',
                                    'rgba(255, 206, 86, 1)',
                                    'rgba(75, 192, 192, 1)',
                                    'rgba(153, 102, 255, 1)',
                                    'rgba(255, 159, 64, 1)'
                                ],
                                borderWidth: 1
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }
                    });
                </script>
            </div>

            <div class="shadow-sm p-3 m-3 ml-1 rounded text-secondary">
                <canvas id="myChart2" width="300" height="300"></canvas>
                <?php
                        $acao=array_keys($arrayT);
                        $find= array();
                        foreach ($acao as $ac) {
                            $find[] = $ac;
                        }
                ?>
                <script>
                    var ctx2 = document.getElementById('myChart2').getContext('2d');
                    var myChart2 = new Chart(ctx2, {
                        type: 'bar',
                        data: {
                            labels: [ 
                                <?php
                                    foreach ($find as $fin) {
                                        echo("'".$fin."'");?> ,
                                    <?php 
                                    }
                                ?>
                            ],
                            datasets: [{
                                label: 'Lucro/Prejuízo em porcentagem',
                                data: [ {{ implode(',', $arrayT)}}],
                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(54, 162, 235, 0.2)',
                                    'rgba(255, 206, 86, 0.2)',
                                    'rgba(75, 192, 192, 0.2)',
                                    'rgba(153, 102, 255, 0.2)',
                                    'rgba(255, 159, 64, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255, 99, 132, 1)',
                                    'rgba(54, 162, 235, 1)',
                                    'rgba(255, 206, 86, 1)',
                                    'rgba(75, 192, 192, 1)',
                                    'rgba(153, 102, 255, 1)',
                                    'rgba(255, 159, 64, 1)'
                                ],
                                borderWidth: 1
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }
                    });
                </script>
                </div>
                <div class="shadow-sm p-3 m-3 ml-1 rounded text-secondary">
                    <canvas id="myChart3" width="300" height="300"></canvas>
                    <?php
                            $acao=array_keys($arrayC);
                            $find2= array();
                            foreach ($acao as $ac) {
                                $find2[] = $ac;
                            }
                    ?>
                    <script>
                        var ctx2 = document.getElementById('myChart3').getContext('2d');
                        var myChart2 = new Chart(ctx2, {
                            type: 'bar',
                            data: {
                                labels: [ 
                                    <?php
                                        foreach ($find2 as $fin) {
                                            echo("'".$fin."'");?> ,
                                        <?php 
                                        }
                                    ?>
                                ],
                                datasets: [{
                                    label: 'Lucro/Prejuízo em reais',
                                    data: [ {{ implode(',', $arrayC)}}],
                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(75, 192, 192, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(255, 159, 64, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255, 99, 132, 1)',
                                        'rgba(54, 162, 235, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(75, 192, 192, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(255, 159, 64, 1)'
                                    ],
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                    </script>
                </div>
            </div>
        @endif

        @if($produtos->count())
            @foreach ($produtos as $produtos)
                <div class="shadow-sm p-3 m-3 bg-white rounded w-100 text-secondary row">
                    <div>
                        <h5>Ação: </h5>
                        <label for="" class="ml-5 mr-5 mt-3">{{ $produtos->atiSigla }} - {{ $produtos->atiNome }}</label>
                    </div>
                    <div class="float-right border-left w-50 text-secondary">
                            <table class="ml-5 border-0 table mr-5 text-secondary">
                                <tr >
                                    <th >Preço Médio</th>
                                    <th></th>
                                    <th>Quantidade</th>
                                    <th>Cotação</th>
                                    <th></th>
                                    <th>Valor</th>
                                    <th></th>
                                    <th>Lucro/Prejuízo</th>
                                </tr>
                                <tr>
                                    <?php
                                       $soma= count($PA);
                                       $tamanho= $soma - 1;
                                       for($i = 0; $i <= $tamanho;$i++){
                                            if($PA[$i] == $produtos->atiSigla){
                                                echo ('<td> R$'.round($PA[$i+1], 2).'<td>');
                                            }
                                       }
                                    ?>
                                    <td>{{ $produtos->Total }}</td>
                                    <?php

                                       for($i = 0; $i <sizeof($PA);$i++){
                                            if($PA[$i] == $produtos->atiSigla){
                                                $Vpa= $PA[$i+1];
                                                $conf = false;
                                                $soma= count($cotacao2);
                                                $tamanho= $soma - 1;
                                                for($i = 0; $i <= $tamanho;$i++){
                                                    foreach($cotacao2[$i] as $indice => $valor){
                                                        if($conf==true){
                                                            $conf=false;
                                                            $final=0;
                                                            echo ('<td>'.$valor.'<td>');//cotação
                                                            $valor = str_replace(",",".", $valor);
                                                            $valor = (float)$valor;//cotação
                                                            $calculo= $produtos->Total * $valor;//valor
                                                            echo ('<td> R$ '.$calculo.'<td>');
                                                            $calculo2= round(($Vpa*$produtos->Total) - $calculo, 2);
                                                            $calculo= str_replace(".",",", $calculo);
                                                            if($calculo2>0){
                                                                echo ('<td class="text-success"> + R$ '.$calculo2.'<td>');
                                                            }else{
                                                                $calculo2= abs($calculo2);
                                                                echo ('<td class="text-danger"> - R$ '.$calculo2.'<td>');
                                                            }
                                                        }
                                                        if($valor == $produtos->atiSigla){
                                                            $conf= true;
                                                        }
                                                    }
                                                }
                                            }
                                       }
                                    ?>
                                </tr>
                            </table>
                    </div>
                </div>
            @endforeach
        @endif
    </div>

@endSection

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>