<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>FUTURE</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
  @stack('stylesheets')

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('css/skins/_all-skins.min.css') }}">
  <!-- Morris chart -->
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ asset('bower_components/jvectormap/jquery-jvectormap.css') }}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
  <!-- bootstrap wysihtml5 - text editor -->
  <!--<link rel="stylesheet" href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')   }}">  -->
  
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
  <link href="{{ asset('css/estilo.css') }}" rel="stylesheet">
  <link href="{{ asset('img/logoMini.png') }}" rel="icon">


<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header fixed-top position-fixed">
    <!-- Logo -->

    <div class="treeview">
      <a href="{{route('home')}}" class="logo bg-dark border-right border-secondary rounded-right" style="height:70px">
        <span class="logo-lg"><b>      
        <img src="{{ asset('img/LOGO.png')}}" alt="Moeda" height="50px" width="160px" class="mr-1 mt-2">
        </b></span>
      </a>
    <div>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-expand-lg bg-dark">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle bg-dark" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu ml-auto">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <!-- User Account: style can be found in dropdown.less -->
          @if(Auth::user()->tipo=='1')
            <li class="nav-item">
              <a class="nav-link" href="{{route('home')}}">Home <span class="sr-only">(current)</span></a>
            </li>
          @endif
          <li class="nav-item dropdown ml-1 mr-1">
              <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-user mr-1"></i>
                {{ Auth::user()->name }}
              </a>
              <!--CONTA-->
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <!--<div>
                  <center>
                    @if(Auth::user()->tipo==1)
                      <label for="" class="text-secondary">Administrador</label>
                    @endif
                  </center>
                </div>-->
                <div class="dropdown-divider"></div>
                <a href="{{ route('enderecos.show', Auth::user()->enderecos_endCodigo ) }}">
                  <button class="dropdown-item btn bg-dark text-light" style="font-size:15px">Visualizar Conta</button>
                </a>
                
                <div class="dropdown-divider"></div>
                
                <form action="{{ route('logout') }}" method="POST">
                  @csrf
                  <button type="submit" class = "dropdown-item btn bg-danger text-light" style="font-size:15px" name = "logout">
                    <center>
                      Sair
                    </center>
                  </button>
                </form>
              </div>
              <!--XX-->
            </li>
            <li>
            
          </li>  
        </ul>
      </div>
    </nav>
  </header>
  <br><br><br>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar position-fixed fixed-left">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
    <br>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header text-secondary" style="font-size:14px">Menu</li>

        
        @if(Auth::user()->tipo=='1')
          <li>
            <a href="{{route('home')}}">
              <img src="{{ asset('img/home3.png')}}" alt="Home" height="21px" width="21px" class="mr-1">
              <span>Home</span>
            </a>
          </li>
          <li class="treeview">
            <a href="#">
              <img src="{{ asset('img/cad.png')}}" alt="Cadastros" height="18px" width="18px" class="mr-1">
              <span>Cadastros</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            
            <ul class="treeview-menu">
              <li><a href="{{ route('instituicao.index') }}"><i class="fa fa-circle-o"></i>Instituições Financeiras</a></li>
              <li><a href="{{ route('ativos.index') }}"><i class="fa fa-circle-o"></i>Ativos</a></li>
            </ul>
          </li>
        @endif
        @if(Auth::user()->tipo=='2')
          <li>
            <a href="{{route('home')}}">
              <img src="{{ asset('img/neg.png')}}" alt="Investimentos" height="20px" width="20px" class="mr-1">
              <span>Meus Investimentos</span>
            </a>
          </li>
          <li>
            <a href="{{ route('produtos.index') }}">
              <img src="{{ asset('img/moeda.png')}}" alt="Moeda" height="18px" width="18px" class="mr-1">
              <span>Compra e Venda</span>
            </a>
          </li>
          <li>
            <a href="{{ route('dividendos.index') }}">
              <img src="{{ asset('img/cash2.png')}}" alt="Cash" height="18px" width="18px" class="mr-1">
              <span>Dividendos e JSCP</span>
            </a>
          </li>
          <li>
            <a href="{{ route('imposto.index') }}">
              <img src="{{ asset('img/tax.png')}}" alt="Renda" height="22px" width="22px" class="mr-1">
              <span>Imposto de Renda</span>
            </a>
          </li>
        @endif
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      @hasSection('body')
        @yield('body')
      @endIf
    </section>

    <!-- Main content -->
    <section class="content">
     

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
    </div>
    <strong>Copyright &copy; 2019.</strong> All rights
    reserved.
  </footer>

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- ./wrapper -->
@stack('scripts')



<!--<script src="{{ url('assets/js/javascript.js') }}"></script>-->

<!-- jQuery 3 -->
<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

<!-- Bootstrap WYSIHTML5 -->
<!--<script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>-->
<!-- Slimscroll -->
<script src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('js/demo.js') }}"></script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>
