@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header bg-dark shadow-sm p-3 mb-5 bg-white rounded">
                    <center>
                        <img src="{{ asset('img/logo.png')}}" alt="Logo" height="75px" width="200px">
                    </center>
                </div>


                <div class="card-body">
                    <center>
                        <h6>Informe os seguintes dados sobre você: </h6>
                    </center>
                
                    <br>


                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row required">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nome') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row required">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" required name="email" value="{{ old('email') }}" autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row required">
                            <label for="cpf" class="col-md-4 col-form-label text-md-right">{{ __('CPF') }}</label>

                            <div class="col-md-6">
                                <input id="cpf" type="cpf" class="form-control @error('cpf') is-invalid @enderror" required name="cpf" value="{{ old('cpf') }}" autocomplete="cpf">

                                @error('cpf')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row required">
                            <label for="cel" class="col-md-4 col-form-label text-md-right">{{ __('Celular') }}</label>

                            <div class="col-md-6">
                                <input id="cel" type="cel" class="form-control @error('cel') is-invalid @enderror" required name="cel" value="{{ old('cel') }}" autocomplete="cel">

                                @error('cel')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="estados" class="col-md-4 col-form-label text-md-right">UF</label>
                            <div class="col-md-6">
                                <select class="form-control dynamic" id="estados" name="estados" onchange="mostraCidade(this)" >
                                    <option value="">Selecione seu Estado</option>
                                    @foreach($estados as $estados)
                                        <option value="{{ $estados->estCodigo}}" >{{ $estados->estNome }} - {{ $estados->estUF}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="cidades_cidCodigo" class="col-md-4 col-form-label text-md-right">Cidade</label>
                            <div class="col-md-6">
                                <select class="form-control dynamic" id="cidades_cidCodigo" name="cidades_cidCodigo">
                                    <option value="">Selecione sua Cidade</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="endRua" class="col-md-4 col-form-label text-md-right">Rua</label>

                            <div class="col-md-6">
                                <input id="endRua" type="text" class="form-control" name="endRua" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="endBairro" class="col-md-4 col-form-label text-md-right">Bairro</label>
                            <div class="col-md-6">
                                <input id="endBairro" type="text" class="form-control" name="endBairro">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="endNumero" class="col-md-4 col-form-label text-md-right">Número</label>
                            <div class="col-md-6">
                                <input id="endNumero" type="text" class="form-control" name="endNumero">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="endComplemento" class="col-md-4 col-form-label text-md-right">Complemento</label>

                            <div class="col-md-6">
                                <input id="endComplemento" type="text" class="form-control" name="endComplemento">
                            </div>
                        </div>

                        <input type="hidden" id="tipo" name="tipo" value="2">

                        <!--Tipo = 1 -> adm
                            Tipo = 2 -> usuario
                        -->

                        <div class="form-group row required">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Senha</label>

                            <div class="col-md-6">
                                <input id="password" type="password" required class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row required">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirme a senha</label>

                            <div class="col-md-6">
                                <input id="password-confirm" required type="password" class="form-control" name="password_confirmation"  autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Cadastrar
                                </button>
                                <a href="{{ route('login') }}">
                                    <button type="button" class="btn btn-dark">Cancelar</button>
                                </a>
                            </div>
                        </div>

                        <div class="form-group offset-md-4">
                            <br>
                            <label for="" class="required text-danger"> Campos Obrigatórios: </label>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>



<script type="text/javascript">
	//var baseurl = window.location.protocol + '//' + window.location.host + '/';
    var baseurl = window.location.protocol + '//' + window.location.host + '/POC/public/estados/';
    console.log(baseurl);

    function mostraCidade(id){

            var estCodigo = id.value;
            console.log(estCodigo);
            $.ajax({
                type: 'GET',
                url: baseurl+estCodigo,
                //url: "http://localhost/POC/public/estados/"+estCodigo,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
				},
                cache: false,
                    error: function(response) {
                        console.log("Erro:\n"+response);
                    },
                    success: function(resposta) {
                        console.log(resposta.cidades);
                        $('#cidades_cidCodigo').empty();
                        for(var i=0; i <resposta.tamanho;i++){
                            $('#cidades_cidCodigo').append('<option value=' + resposta.cidades[i].cidCodigo + '>' + resposta.cidades[i].cidNome + '</option>');
                        }
                        
                    }		
                });

    }

    jQuery(function($){
        $("#cel").mask("(99) 99999-9999");
        $("#cpf").mask("999.999.999-99");
    });
</script>
