@extends('layouts.master')

@section('body')
    <div class="row border-bottom border-secondary rounded-bottom p-3 shadow-sm">
        <img src="{{ asset('img/barras.png')}}" alt="Ações" height="40px" width="40px" class="mr-3">
        <h2 class="text-secondary">Editar Instituição Financeira</h2>
    </div>
    <br><br>

    <div>
        <form name="formIns" id="formIns" method="POST" action="{{ route('instituicao.update',$instituicao->insCodigo) }}">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="text-secondary">Nome:</label>
                <input type="text" class="form-control border rounded" id="insNome" name = "insNome" placeholder="Insira o nome da Instituição Financeira" value="{{$instituicao->insNome}}">
                @if($errors->get('insNome'))
                    @foreach($errors->get('insNome') as $error)
                        <span class="text-danger">{{ $error }}</span>
                    @endForeach
                @endIF
            </div>
            <div class="form-group">
                <label class="text-secondary">Descrição:</label>
                <input type="text" class="form-control border rounded" id="insDescricao" name = "insDescricao" placeholder="Insira a descrição da Instituição Financeira" value="{{$instituicao->insDescricao}}">
                </textarea>
                @if($errors->get('insDescricao'))
                    @foreach($errors->get('insDescricao') as $error)
                        <span class="text-danger">{{ $error }}</span>
                    @endForeach
                @endIF
            </div>
            <div class="row">
                <button type="submit" class="btn btn-success ml-auto">Salvar Alterações</button>
                <a class="ml-3 mr-4 btn btn-dark" href="{{ route('instituicao.index') }}">
                        Cancelar
                </a>
            </div>
        </form>
    </div>

@endSection