@extends('layouts.master')

@section('body')

<div class="row border-bottom border-secondary rounded-bottom p-3 shadow-sm">
    <img src="{{ asset('img/barras.png')}}" alt="Ações" height="40px" width="40px" class="mr-3">
    <h2 class="text-secondary">Adicionar Nova Instituição Financeira</h2>
</div>
    <br><br>

<div>
    <form name="formIns" id="formIns" method="POST" action="{{url('instituicao')}}"> <!--Manda pro store do controller-->
        @csrf
        <div class="form-group">
            <label class="text-secondary required">Nome:</label>
            <input type="text" class="form-control border rounded" id="insNome" name = "insNome" placeholder="Insira o nome da Instituição Financeira">
            @if($errors->get('insNome'))
                @foreach($errors->get('insNome') as $error)
                    <span class="text-danger">{{ $error }}</span>
                @endForeach
            @endIF
        </div>
        <div class="form-group">
            <label class="text-secondary">Descrição:</label>
            <input type="text" class="form-control border rounded" id="insDescricao" name = "insDescricao" placeholder="Insira a descrição da Instituição Financeira">
        </div>
        <div class="form-group">
            <label for="" class="required text-danger"> Campos Obrigatórios: </label>
        </div>

        <div class="row">
            <button type="submit" class="btn btn-primary ml-auto">Cadastrar</button>
            <a class="ml-3 mr-4 btn btn-dark" href="{{ route('instituicao.index') }}">
                    Cancelar
            </a>
        </div>
    </form>
</div>

@endSection