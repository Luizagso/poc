@extends('layouts.master')

@section('body')

<div class="row border-bottom border-secondary rounded-bottom p-3 shadow-sm">
    <img src="{{ asset('img/barras.png')}}" alt="Ações" height="40px" width="40px" class="mr-3">
    <h2 class="text-secondary">Editar Ação</h2>
</div>
    <br><br>

<div>
    <form name="formPro" id="formPro" method="POST" action="{{ route('produtos.update',$produtos->proCodigo) }} "> 
        @method('PUT')
        @csrf
        <div class="row"> 
            <div class="w-25 ml-5">
                <div class="form-group ">
                    <label class="text-secondary required">Instituição Financeira:</label>
                    <select class="form-control border rounded" name="instituicao_insCodigo" id="instituicao_insCodigo" required>
                        <option value="{{ $produtos->instituicao->insCodigo}}" class="text-light bg-secondary">{{ $produtos->instituicao->insNome }}</option>
                        @foreach($instituicao as $instituicao)
                            <option value="{{ $instituicao->insCodigo}}" >{{ $instituicao->insNome }}</option>
                        @endforeach
                    </select>
                    @if($errors->get('instituicao_insCodigo'))
                        @foreach($errors->get('instituicao_insCodigo') as $error)
                            <span class="text-danger">{{ $error }}</span>
                        @endForeach
                    @endIF
                </div>
                <div class="form-group">
                    <label class="text-secondary required">Produto:</label>
                    <select class="form-control border rounded" name="ativos_atiCodigo" id="ativos_atiCodigo" required>
                    <option value="{{ $produtos->ativos->atiCodigo}}" class="text-light bg-secondary">{{ $produtos->ativos->atiSigla }} - {{ $produtos->ativos->atiNome }}</option>
                        @foreach($ativos as $ativos)
                            <option value="{{ $ativos->atiCodigo}}" >{{ $ativos->atiSigla }} - {{ $ativos->atiNome}}</option>
                        @endforeach
                    </select>
                    @if($errors->get('ativos_atiCodigo'))
                        @foreach($errors->get('ativos_atiCodigo') as $error)
                            <span class="text-danger">{{ $error }}</span>
                        @endForeach
                    @endIF
                </div>
                <div class="form-group">
                    <label class="text-secondary required">Operação:</label>
                    <select name="proOperacao" id="proOperacao" class="form-control border rounded" required>
                        @if($produtos-> proOperacao==1)
                            <option value="1" class="bg-secondary text-light">Compra</option>
                        @else
                            <option value="2" class="bg-secondary text-light">Venda</option>
                        @endif
                            <option value="1">Compra</option>
                            <option value="2">Venda</option>
                    </select>
                    @if($errors->get('proOperacao'))
                        @foreach($errors->get('proOperacao') as $error)
                            <span class="text-danger">{{ $error }}</span>
                        @endForeach
                    @endIF
                </div>
            </div>
            <div class="w-25 ml-5">
                <div class="form-group">
                    <label class="text-secondary required">Preço:</label>
                    <input type="text" class="form-control border rounded" id="proPreco" name = "proPreco" placeholder="Insira o preço da ação" required value="{{ $produtos->proPreco}}">
                    @if($errors->get('proPreco'))
                        @foreach($errors->get('proPreco') as $error)
                            <span class="text-danger">{{ $error }}</span>
                        @endForeach
                    @endIF
                </div>
                <div class="form-group">
                    <label class="text-secondary required">Quantidade Comprada:</label>
                    <input type="text" class="form-control border rounded" id="proQuantidade" name = "proQuantidade" placeholder="Insira a quantidade comprada" required value="{{ $produtos->proQuantidade}}">
                    @if($errors->get('proQuantidade'))
                        @foreach($errors->get('proQuantidade') as $error)
                            <span class="text-danger">{{ $error }}</span>
                        @endForeach
                    @endIF
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="text-secondary ml-3">Taxas:</label>
                        <label for="" class="text-secondary ml-1 mt-1" style="font-size:13px">
                            (corretagem + emolumentos + liquidação)
                        </label>
                    </div>
                    <input type="text" class="form-control border rounded" id="proTaxa" name = "proTaxa" placeholder="Insira a taxa" value="{{ $produtos->proTaxa}}">
                </div>
                <input type="hidden"  name="users_id" id="users_id" value="{{ Auth::user()->id }}">
            </div>
            <div class="w-25 ml-5">
                <div class="form-group">
                    <label class="text-secondary required">Data:</label>
                    <input type="date" class="form-control border rounded" id="proData" name = "proData" placeholder="Insira a data da compra da ação" value="{{ $produtos->proData}}" required>
                    @if($errors->get('proData'))
                        @foreach($errors->get('proData') as $error)
                            <span class="text-danger">{{ $error }}</span>
                        @endForeach
                    @endIF
                </div>

                <div class="mt-5">
                        <button type="submit" class="btn btn-success mt-auto w-100">Salvar</button>
                        <br><br><br>
                        <a href="{{ route('produtos.index') }}" class="btn btn-dark w-100">
                            Cancelar
                        </a>
                </div>
            </div>
        </div>

        <div class="ml-5">
            <br>
            <label for="" class="required text-danger"> Campos Obrigatórios: </label>
        </div>
    </form>
</div>

@endSection

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>


<script type="text/javascript">
    jQuery(function($){
            $("#proPrecoCompra").mask('#.##0.00', {reverse: true});
            $("#proTaxa").mask('#.##0.00', {reverse: true});
    });
</script>