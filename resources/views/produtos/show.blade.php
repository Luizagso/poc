@extends('layouts.master')

@section('body')

<div class="row border-bottom border-secondary rounded-bottom p-3 shadow-sm">
    <img src="{{ asset('img/barras.png')}}" alt="Ações" height="40px" width="40px" class="mr-3">
    <h2 class="text-secondary">Visualizar {{$produtos->ativos->atiSigla }}</h2>
</div>
    <br><br>

<div>
<form name="formPro" id="formPro" method="POST" action=" ">
        @csrf
        <div class="row"> 
            <div class="w-25 ml-5">
                <div class="form-group ">
                    <label class="text-secondary">Instituição Financeira:</label>
                    <input disabled="disabled" class="form-control border rounded" id="instituicao_insCodigo" name = "instituicao_insCodigo" value="{{ $produtos->instituicao->insNome }}">
                </div>
                <div class="form-group">
                    <label class="text-secondary">Ativo:</label>
                    <input disabled="disabled" class="form-control border rounded" id="ativos_atiCodigo" name = "ativos_atiCodigo" value="{{ $produtos->ativos->atiSigla }} - {{ $produtos->ativos->atiNome }}">
                </div>
                <div class="form-group">
                    <label class="text-secondary">Operação:</label>
                    @if($produtos-> proOperacao==1)
                        <input disabled="disabled" class="form-control border rounded" id="proOperacao" name = "proOperacao" value="Compra">
                        @else
                            <input disabled="disabled" class="form-control border rounded" id="proOperacao" name = "proOperacao" value="Venda">
                    @endif
                </div>
            </div>
            <div class="w-25 ml-5">
                <div class="form-group">
                    <label class="text-secondary">Preço:</label>
                    <input disabled="disabled" class="form-control border rounded" id="proPreco" name = "proPreco" value="R${{ $produtos-> proPreco}}">
                </div>
                <div class="form-group">
                    <label class="text-secondary">Quantidade:</label>
                    <input disabled="disabled" class="form-control border rounded" id="proQuantidade" name = "proQuantidade" value="{{ $produtos-> proQuantidade }}">
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="text-secondary ml-3">Taxas:</label>
                        <label for="" class="text-secondary ml-1 mt-1" style="font-size:13px">
                           (corretagem + emolumentos + liquidação)
                        </label>
                    </div>
                    @if($produtos->proTaxa==NULL)
                        <input disabled="disabled" class="form-control border rounded" id="proTaxa" name = "proTaxa" value="Nenhuma taxa foi aplicada">
                    @else
                        <input disabled="disabled" class="form-control border rounded" id="proTaxa" name = "proTaxa" value="R${{ $produtos-> proTaxa}}">
                    @endif
                </div>
                <input type="hidden"  name="users_id" id="users_id" value="{{ Auth::user()->id }}">
            </div>
            <div class="w-25 ml-5">
                <div class="form-group">
                    <label class="text-secondary">Data:</label>
                    <input disabled="disabled" class="form-control border rounded" id="proData" name = "proData" value="{{ $produtos->proData }}" type="date">
                </div>
                <div class="mt-5">
                        <a href="{{ route('produtos.index') }}" class="btn btn-dark w-25 ml-4">
                            Voltar
                        </a>
                        <a href="{{ route('produtos.edit', $produtos->proCodigo) }}" class="btn btn-primary w-25 ml-1">
                            Editar
                        </a>
                        <a href=" " class="btn btn-danger w-25 ml-1" data-toggle="modal" data-target="#modalExcluir">
                            Excluir
                        </a>
                </div>

            </div>
        </div>
    </form>
</div>

   <!--MODAL-->
   <div class="modal fade" tabindex="-1" role="dialog" id="modalExcluir">
        <div class="modal-dialog border rounded" role="document">
            <div class="modal-content border rounded">
                <div class="modal-header bg-danger" style="height:50px;">
                    <h5 class="modal-title text-light align-center">ALERTA!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <input type="hidden" id="codigo">
                <div class="modal-body bg-light">
                        <span>Deseja</span>
                        <span class="text-danger">excluir</span>
                        <span>a ação?</span>
                        <p id="modalBody"> </p>
                </div>
                <div class="modal-footer bg-light" style="height:60px;">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                    <form action="{{ route('produtos.destroy', $produtos-> proCodigo )}}" method="POST">
                    @method('DELETE')
                    @csrf
                        <button type="submit" class="btn btn-danger mt-3">Excluir</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endSection

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>

