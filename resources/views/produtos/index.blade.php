@extends('layouts.master')

@section('body')

<!--Mensagem success-->
@if(Session::has('success'))
	<div class="alert alert-success alert-dimissible fade show">
		{{ Session::get('success') }}
		<button type="button" class="close" data-dismiss='alert' arial-label="Close">
			<span aria-hidden = "true"> &times; </span>
		</button>
	</div>
@endIF

<!--CABEÇALHO-->
<div class="row border-bottom border-secondary rounded-bottom p-3 shadow-sm">
    <img src="{{ asset('img/barras.png')}}" alt="Ações" height="40px" width="40px" class="mr-3">
    <h2 class="text-secondary">Ações Compradas e Vendidas</h2>
    <a class="ml-auto mr-2" href="{{ route('produtos.create') }}">
        <button class="btn btn-success">
            Registrar Operação
        </button>
    </a>
    <a class="mr-2" href="{{ route('pdf.produtos') }}">
        <button class="btn btn-dark text-light">
            PDF
        </button>
        <!-- Quando clicar aqui tem que gerar o pdf-->
    </a>
</div>

<br><br>

<!--TABELA-->
<div class="">
        <table class="table table-dark text-dark" id="produtos-table">
            <thead>
                <tr>
                    <th class="text-light">Operação</th>
                    <th class="text-light">Ação</th>
                    <th class="text-light">Quantidade </th>
                    <th class="text-light">Valor </th>
                    <th class="text-light">Total </th>
                    <th class="text-light">Opções </th>
                </tr>
                <tbody>
                    @foreach ($produtos as $pro)
                        <tr>
                            <input type="hidden" value="{{$pro->proCodigo}}" name="proCodigo" id="proCodigo">
                            @if($pro->proOperacao==1)
                                <td>Compra</td>
                            @else ($pro->proOperacao==2)
                                <td>Venda</td>
                            @endif
                            <td>{{$pro->ativos->atiSigla}}</td>
                            <td>{{$pro->proQuantidade}}</td>
                            <td>R${{$pro->proPreco}}</td>
                            <td>R${{$pro->proQuantidade * $pro->proPreco}}</td>
                            <td>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <a href="{{ route('produtos.show',$pro->proCodigo) }}" class="btn btn-dark" style="height:40px;">
                                            Visualizar
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </thead>
        </table>
</div>
@endSection


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script type="text/javascript">

    jQuery(function() {
                jQuery('#produtos-table').DataTable({ 
                    dom: 'lBfrtip',
                    "language": {
                        "url": "http://cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
                    },
                    buttons: [
                        'pdfHtml5',
                        'excelHtml5'
                    ]
                });
    });

  </script>
