<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PDF</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">  
    <link href="{{ asset('img/logoMini.png') }}" rel="icon">

</head>
<body>

    <div class="w-100" >
        <img src="{{ asset('img/LOGO.png')}}" alt="Logo" height="60px" width="150px" style="margin-left:280px">
        <br>
        <h5 style="margin-left:170px" class="mt-4">Relatório de investimentos em renda variável</h5>
    </div>
    <br>
    <div class="p-2">
        <label for="" class="ml-2">Ações compradas:</label>
        <br>
        <table class="table ml-3 mr-3 border">
        <thead>
                    <tr>
                        <th class="border" style="font-size: 13px">Ação</th>
                        <th class="border" style="font-size: 13px">Instituição Financeira</th>
                        <th class="border" style="font-size: 13px">Data</th>
                        <th class="border" style="font-size: 13px">Quantidade </th>
                        <th class="border" style="font-size: 13px">Valor </th>
                        <th class="border" style="font-size: 13px">Taxa </th>
                        <th class="border" style="font-size: 13px">Total </th>
                    </tr>
                    <tbody>
                        @foreach ($produtos as $pro)
                            @if($pro->proOperacao==1)
                                <tr>
                                    <td class="border" style="font-size: 13px">{{$pro->ativos->atiSigla}} - {{$pro->ativos->atiNome}}</td>
                                    <td class="border" style="font-size: 13px">{{$pro->instituicao->insNome}}</td>
                                    <td class="border" style="font-size: 13px" type="date">{{$pro->proData}}</td>
                                    <td class="border" style="font-size: 13px">{{$pro->proQuantidade}}</td>
                                    <td class="border" style="font-size: 13px">R${{$pro->proPreco}}</td>
                                    @if($pro->proTaxa==NULL)
                                        <td class="border" style="font-size: 13px"> - </td>
                                    @else
                                        <td class="border" style="font-size: 13px">R${{$pro->proTaxa}}</td>
                                    @endif
                                    <td class="border" style="font-size: 13px"> R${{$pro->proQuantidade * $pro->proPreco}}</td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
            </thead>
        </table>
        <label for="" class="ml-2">Valor total: R${{ $TotalCompras}}</label>
    
    </div>
    <br><br>
    <div class="p-2">
        <label for="" class="ml-2">Ações vendidas:</label>
        <br>

        <table class="table ml-3 mr-3 border">
            <thead>
                    <tr>
                        <th class="border" style="font-size: 13px">Ação</th>
                        <th class="border" style="font-size: 13px">Instituição Financeira</th>
                        <th class="border" style="font-size: 13px">Data</th>
                        <th class="border" style="font-size: 13px">Quantidade </th>
                        <th class="border" style="font-size: 13px">Valor </th>
                        <th class="border" style="font-size: 13px">Taxa </th>
                        <th class="border" style="font-size: 13px">Total </th>
                    </tr>
                    <tbody>
                        @foreach ($produtos as $pro)
                            @if($pro->proOperacao==2)
                                <tr>
                                    <td class="border" style="font-size: 13px">{{$pro->ativos->atiSigla}} - {{$pro->ativos->atiNome}}</td>
                                    <td class="border" style="font-size: 13px">{{$pro->instituicao->insNome}}</td>
                                    <td class="border" style="font-size: 13px" type="date">{{$pro->proData}}</td>
                                    <td class="border" style="font-size: 13px">{{$pro->proQuantidade}}</td>
                                    <td class="border" style="font-size: 13px">R${{$pro->proPreco}}</td>
                                    @if($pro->proTaxa==NULL)
                                        <td class="border" style="font-size: 13px"> - </td>
                                    @else
                                        <td class="border" style="font-size: 13px">R${{$pro->proTaxa}}</td>
                                    @endif
                                    <td class="border" style="font-size: 13px"> R${{$pro->proQuantidade * $pro->proPreco}}</td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
            </thead>
        
        
        </table>
        <label for="" class="ml-2">Valor total: R${{ $TotalVendas}}</label>
    
    </div>

    <div class="ml-3 mr-3">
        <label for="" style="margin-left:280px">{{$data}}</label>
    </div>
</body>
</html>