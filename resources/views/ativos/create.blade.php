@extends('layouts.master')

@section('body')

<div class="row border-bottom border-secondary rounded-bottom p-3 shadow-sm">
    <img src="{{ asset('img/barras.png')}}" alt="Ações" height="40px" width="40px" class="mr-3">
    <h2 class="text-secondary">Adicionar Novo Ativo</h2>
</div>
    <br><br>

<div>
    <form name="formAti" id="formAti" method="POST" action="{{url('ativos')}}">
        @csrf
        <div class="form-group">
            <label class="text-secondary required">Nome:</label>
            <input type="text" class="form-control border rounded" id="atiNome" name = "atiNome" placeholder="Insira o nome do Ativo">
            @if($errors->get('atiNome'))
                @foreach($errors->get('atiNome') as $error)
                    <span class="text-danger">{{ $error }}</span>
                @endForeach
            @endIF
        </div>
        <div class="form-group">
            <label class="text-secondary required">Sigla:</label>
            <input type="text" class="form-control border rounded" id="atiSigla" name = "atiSigla" placeholder="Insira a sigla do Ativo">
            @if($errors->get('atiSigla'))
                @foreach($errors->get('atiSigla') as $error)
                    <span class="text-danger">{{ $error }}</span>
                @endForeach
            @endIF
        </div>
        <div class="form-group">
            <label class="text-secondary required">CNPJ:</label>
            <input type="text" class="form-control border rounded" id="atiCNPJ" name = "atiCNPJ" placeholder="Insira o CNPJ do Ativo">
            @if($errors->get('atiCNPJ'))
                @foreach($errors->get('atiCNPJ') as $error)
                    <span class="text-danger">{{ $error }}</span>
                @endForeach
            @endIF
        </div>
        <div class="form-group">
            <label for="" class="required text-danger"> Campos Obrigatórios: </label>
        </div>

        <div class="row">
            <button type="submit" class="btn btn-primary ml-auto">Cadastrar</button>
            <a class="ml-3 mr-4 btn btn-dark" href="{{ route('ativos.index') }}">
                    Cancelar
            </a>
        </div>
    </form>
</div>

@endSection


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>

<script type="text/javascript">
    jQuery(function($){
            $("#atiCNPJ").mask("99.999.999/9999-99");
    });
</script>