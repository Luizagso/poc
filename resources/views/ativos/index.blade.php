@extends('layouts.master')

@section('body')

<!--Mensagem success-->
@if(Session::has('success'))
	<div class="alert alert-success alert-dimissible fade show">
		{{ Session::get('success') }}
		<button type="button" class="close" data-dismiss='alert' arial-label="Close">
			<span aria-hidden = "true"> &times; </span>
		</button>
	</div>
@endIF

<!--CABEÇALHO-->
<div class="row border-bottom border-secondary rounded-bottom p-3 shadow-sm">
    <img src="{{ asset('img/barras.png')}}" alt="Ações" height="40px" width="40px" class="mr-3">
    <h2 class="text-secondary">Ativos</h2>
    <a class="ml-auto mr-2" href="{{ route('ativos.create') }}">
        <button class="btn btn-success">
            Adicionar
        </button>
    </a>
</div>

<br><br>

<!--TABELA-->
<div class="">
        <table class="table table-dark text-dark" id="ativos-table">
            <thead>
                <tr>
                    <th class="text-light ">Nome</th>
                    <th class="text-light ">Sigla</th>
                    <th class="text-light ">CNPJ</th>
                    <th class="text-light ">Opções </th>
                </tr>
            </thead>
        </table>
</div>

<!--MODAL-->
    <div class="modal fade" tabindex="-1" role="dialog" id="modalExcluir">
        <div class="modal-dialog border rounded" role="document">
            <div class="modal-content border rounded">
                <div class="modal-header bg-danger" style="height:50px;">
                    <h5 class="modal-title text-light align-center">ALERTA!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <input type="hidden" id="codigo">
                <div class="modal-body bg-light">
                        <span>Deseja</span>
                        <span class="text-danger">excluir</span>
                        <span>o item selecionado?</span>
                        <p id="modalBody"> </p>
                </div>
                <div class="modal-footer bg-light">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-danger" onclick="excluir()">Excluir</button>
                </div>
            </div>
        </div>
    </div>

@endSection

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script type="text/javascript">

function abrirModal(data) {
        var insAtivos= data;
        console.log(insAtivos);
        $.ajax({
            type:'GET',
            url:'ativos',
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: false,
                error: function(response) {
                    console.log("Erro:\n"+response);
                },
                success: function(resposta) {
                    if(resposta != 'Vazio'){
                        codigo.value = insAtivos;
                        //modalBody.innerHTML = "Deseja excluir o item selecionado?";
                        $('#modalExcluir').modal('show');
                    } else {
                        alert('Não é possível realizar a exclusão');
                    }
                }
        });
    }

    function excluir(){        
        var insAtivos = codigo.value;
        console.log(insAtivos);

        $.ajax({
            type:'DELETE',
            url: 'ativos/'+insAtivos,
            //data: "{}",
            //contentType: "application/json; charset=utf-8",
            //dataType: "json",
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Content-type': 'application/json',
                'Authorization': '${this.state.tokenType} ${this.state.token}' 
            },
            cache: false,
                error: function(response) {
                    console.log("Erro:\n"+response);
                },
                success: function(resposta) {
                    if(resposta == true){
                        $('#modalExcluir').modal('hide');
                        console.log("ok");
                        window.location.assign('ativos');
                    } else {
                        alert('Não é possível realizar a exclusão');
                    }
                }
        }); 
    }


jQuery(function() {
            jQuery('#ativos-table').DataTable({ //aqui é o id da minha datatable
                dom: 'lBfrtip',
                "ajax": {
                    "url": '{{ route("ativos.datatable") }}', //aqui é a rota que vai me levar até o metodo
                    "type": "GET"
                },
                "columns": [{
                        data: 'atiNome', // aqui tem que ser o nome igual do banco e na ordem dos TH 
                        name: 'Nome',
                    },
                    {
                        data: 'atiSigla',
                        name: 'Sigla'
                    },
                    {
                        data: 'atiCNPJ',
                        name: 'CNPJ'
                    },
                    {
                        data: "atiCodigo", //aqui estou passando a chave primária para meus buttons
                        render: function(data, type, row) {

                            var html= '<div class="row">';
                            html += '<div class="col-lg-6">';
                            html += '<a href="ativos/' + data + '/edit" class="btn btn-primary" style="height:40px;">';
                            html += '<i class="fa fa-edit mr-1"></i>';
                            html += 'Editar';
                            html += '</a>';
                            html += '</div>';
                            html += '<div class="col-lg-6">';
                            html += '<button type="button" class="btn btn-danger" style="height:40px;" data-toggle="modal" data-target="#modalExcluir" onclick="abrirModal('+data+')"> ';
                            html += '<i class="fa fa-trash-o mr-2"></i>';
                            html += 'Apagar';
                            html += '</button>';
                            html += '</div>';
                            html += '</div>';
                            return html;},
                        name: 'Opções'
                    }
                ],
                "language": {
                    "url": "http://cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
                },
                buttons: [
                    'pdfHtml5',
                    'excelHtml5'
                ]
            });
        });
  </script>