@extends('layouts.master')
@section('body')

    <div class="row justify-content-center mt-3">
        <img src="{{ asset('img/LOGO.png')}}" alt="Moeda" height="90px" width="255px">
    </div>

    <div class="row justify-content-center mt-5">
        <h2 class="text-secondary justify-content-center mt-3">Olá Administrador!</h2> <br>
    </div>
    @foreach ($usuarios as $usu)
        <div class="row justify-content-center">
            <h3 class="text-secondary justify-content-center mt-2">Você possui {{$usu->Total - 1}} investidor(es) utilizando o seu sistema!</h3> 
        </div>
    @endforeach
    

@endSection