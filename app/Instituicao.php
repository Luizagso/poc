<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instituicao extends Model
{
    protected $table= 'instituicao';
    protected $primaryKey= 'insCodigo';
    protected $fillable= ['insNome', 'insDescricao'];
}
