<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dividendos extends Model
{
    protected $table= 'dividendos';
    protected $primaryKey= 'divCodigo';
    protected $fillable= ['divData', 'divValor', 'divTipo'];
}
