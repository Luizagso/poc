<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Enderecos;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'cpf' => ['required'],
            'cel' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
            $result = new Enderecos;

            if($data['endBairro']==NULL){
                $result->endBairro = NULL;
            } else {
                $result->endBairro = $data['endBairro'];
            }

            if($data['endRua']==NULL){
                $result->endRua = NULL;
            } else {
                $result->endRua = $data['endRua'];
            }

            if($data['endNumero']==NULL){
                $result->endNumero = NULL;
            } else {
                $result->endNumero = $data['endNumero'];
            }

            if($data['endComplemento']==NULL){
                $result->endComplemento = NULL;
            } else {
                $result->endComplemento = $data['endComplemento'];
            }
            
            
            if($data['cidades_cidCodigo']==NULL){
                $result->cidades_cidCodigo = NULL;
            } else {
                $result->cidades_cidCodigo = $data['cidades_cidCodigo'];
            }

            $result->save();

            return User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'cpf' => $data['cpf'],
                'cel' => $data['cel'],
                'tipo' => $data['tipo'], 
                'enderecos_endCodigo' => $result->endCodigo,
                'password' => Hash::make($data['password']),
            ]);
    }
}
