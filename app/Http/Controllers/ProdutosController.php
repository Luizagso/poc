<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produtos;
use App\Ativos;
use App\Http\Requests\ProdutosRequest;
use Illuminate\Support\Facades\DB;
use Auth;
use PDF;
Use \Carbon\Carbon;

class ProdutosController extends Controller
{

    private $objProdutos;

    public function __construct(){
        $this-> objProdutos = new Produtos;

        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produtos = Produtos::where('users_id', Auth::user()->id)->get();
        return view('produtos.index',['produtos'=>$produtos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ativos = DB::table('ativos')->orderBy('atiNome', 'asc')
         ->get();

        $instituicao = DB::table('instituicao')->orderBy('insNome', 'asc')
         ->get();

        return view('produtos.create', compact('ativos', 'instituicao'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProdutosRequest $request)
    {
        //Operação = 1-Compra 2-Venda
        $cadastro = $this->objProdutos->create([
            'proData' =>$request->proData,
            'proQuantidade' =>$request->proQuantidade,
            'proPreco' => ((float) $request->proPreco),
            'proTaxa' =>$request->proTaxa,
            'proOperacao' =>$request->proOperacao,
            'ativos_atiCodigo' =>$request->ativos_atiCodigo,
            'users_id' =>$request->users_id,
            'instituicao_insCodigo' =>$request->instituicao_insCodigo
        ]);
        if($cadastro){
            return redirect()->route('produtos.index')
            ->withSuccess('Compra de ativo registrada!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produtos = $this->objProdutos->find($id);
        return view('produtos.show', compact('produtos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ativos = DB::table('ativos')->orderBy('atiNome', 'asc')
         ->get();

        $instituicao = DB::table('instituicao')->orderBy('insNome', 'asc')
         ->get();

        $produtos = $this->objProdutos->find($id);

        return view('produtos.update', compact('produtos', 'ativos','instituicao'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProdutosRequest $request, $id)
    {
        //Operação = 1-Compra 2-Venda
        $cadastro = $this->objProdutos->where(['proCodigo'=>$id])->update([
            'proData' =>$request->proData,
            'proQuantidade' =>$request->proQuantidade,
            'proPreco' => ((float) $request->proPreco),
            'proTaxa' =>$request->proTaxa,
            'proOperacao' =>$request->proOperacao,
            'ativos_atiCodigo' =>$request->ativos_atiCodigo,
            'users_id' =>$request->users_id,
            'instituicao_insCodigo' =>$request->instituicao_insCodigo
        ]);
        if($cadastro){
            return redirect()->route('produtos.index')
            ->withSuccess('Ativo alterado com sucesso!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produtos = Produtos:: find($id);
        $produtos->delete();

        return redirect()->route('produtos.index');
    }

    public function dataTableJson(){
        return response()->json(['data'=>Produtos::where('users_id', Auth::user()->id)->with('ativos')->get()]);
    }

    public function gerarPDF(){
        
        $data = date("d/m/Y");
        $data = explode("/", $data);
        list($dia, $mes, $ano) = $data;

        if($mes == 1){ $mes="janeiro"; }
        if($mes == 2){ $mes="fevereiro"; }
        if($mes == 3){ $mes="março"; }
        if($mes == 4){ $mes="abril"; }
        if($mes == 5){ $mes="maio"; }
        if($mes == 6){ $mes="junho"; }
        if($mes == 7){ $mes="julho"; }
        if($mes == 8){ $mes="agosto"; }
        if($mes == 9){ $mes="setembro"; }
        if($mes == 10){ $mes="outubro"; }
        if($mes == 11){ $mes="novembro"; }
        if($mes == 12){ $mes="dezembro"; }

        $data= "$dia de $mes de $ano";

        $produtosC= Produtos::where('users_id', Auth::user()->id)
        ->where('proOperacao',1)->get();

        $produtosV= Produtos::where('users_id', Auth::user()->id)
        ->where('proOperacao',2)->get();

        $produtos= Produtos::where('users_id', Auth::user()->id)->get();

        $TotalCompras= 0;
        $Calculo = 0;

        foreach($produtosC as $produtosC){
            $Soma= $TotalCompras;
            $Calculo= $produtosC->proQuantidade * $produtosC->proPreco;
            $TotalCompras= $Soma + $Calculo ; 
        }

        $TotalVendas= 0;
        $Calculo2 = 0;

        foreach($produtosV as $produtosV){
            $Soma= $TotalVendas;
            $Calculo2= $produtosV->proQuantidade * $produtosV->proPreco;
            $TotalVendas= $Soma + $Calculo2 ;   
        }
        
        $pdf = PDF::loadView('produtos.pdf', compact('produtos','data', 'TotalCompras', 'TotalVendas')); //espera uma view

        return $pdf->setPaper('a4')->stream('ativos.pdf');
    }


}
