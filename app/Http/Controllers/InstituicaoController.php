<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Instituicao;
use App\Http\Requests\InstituicaoRequest;

class InstituicaoController extends Controller
{
    private $objInstituicao;

    public function __construct(){
        $this-> objInstituicao = new Instituicao;

        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $instituicao= $this->objInstituicao->all();
        return view('instituicao.index',compact('instituicao'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('instituicao.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InstituicaoRequest $request)
    {
        Instituicao:: create([
            'insNome' =>$request->insNome,
            'insDescricao' =>$request->insDescricao
        ]);
            return redirect()->route('instituicao.index')
            ->withSuccess('Instituição Financeira cadastrada!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $instituicao = $this->objInstituicao->find($id);
        return view('instituicao.update', compact('instituicao'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InstituicaoRequest $request, $id)
    {
        $this->objInstituicao->where(['insCodigo'=>$id])->update([
            'insNome' =>$request->insNome,
            'insDescricao' =>$request->insDescricao
        ]);
        return redirect()->route('instituicao.index')
        ->withSuccess('Instituição Financeira editada com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $instituicao = Instituicao::find($id);
        if ( $instituicao == null) {
            return response()->json(false);
        }
        $instituicao->delete();
        return response()->json(true);
        
    }

    public function dataTableJson(){
        return response()->json(['data'=>Instituicao::all()]);
    }
}
