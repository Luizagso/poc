<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Produtos;
use App\User;
use Illuminate\Support\Facades\DB;
use PDF;
use App\Services\SheetService;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(SheetService $sheetService)
    {
        if(Auth::user()->tipo==2){

            $produtos= Produtos:: join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
            ->select( DB::raw('sum( produtos.proQuantidade ) as Total, sum( produtos.proPreco ) as Preco, produtos.proQuantidade, produtos.proPreco, count(produtos.proCodigo) as TotalId, at.atiSigla, at.atiNome'))
            ->groupBy('produtos.ativos_atiCodigo')
            ->where('users_id', Auth::user()->id)
            ->get(); 

            $cotacao2 = $sheetService->get();

            $conf = false;
            $soma= count($cotacao2);
            $total= count($produtos);
            $tamanho= $soma - 1;
            $array= [];
            $arrayC = [];
            $arrayT=[];
            $nome= "";


            $PM= Produtos:: join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
            ->select( DB::raw('produtos.proQuantidade, produtos.proPreco, at.atiSigla'))
            ->where('users_id', Auth::user()->id)
            ->get(); 

            $PA= [];

            foreach($PM as $prod){
                $quantidadeTotal = 0 ;
                $soma=0;
                $preco= 0;
                $pm=0;
                $final=0;
                foreach($PM as $p){
                    if($prod->atiSigla == $p->atiSigla){
                        $quantidadeTotal =  $soma + $p->proQuantidade;
                        $preco= $pm + ($p->proPreco *$p->proQuantidade);
                    }
                    $soma= $quantidadeTotal;
                    $pm= $preco;
                    if($pm>0){
                        $final= $pm/$soma;
                    }
                }
                if(array_search ($prod->atiSigla, $PA) == true){
                }else{
                    $PA[] = $prod->atiSigla;
                    $PA[] = $final;
                }
            }
            //dd($PA);

            if($total>0){
                foreach ($produtos as $prod) {
                    for($i = 0; $i <sizeof($PA);$i++){
                        if($PA[$i] == $prod->atiSigla){
                            $Vpa= $PA[$i+1];
                            $conf = false;
                            $final=0;
                            $soma= count($cotacao2);
                            $tamanho= $soma - 1;
                            for($i = 0; $i <= $tamanho;$i++){
                                foreach($cotacao2[$i] as $indice => $valor){
                                    if($conf==true){
                                        $valor = str_replace(",",".", $valor);
                                        $valor = (float)$valor;//cotação
                                        $calculo= $prod->Total * $valor; //valor
                                        $calculo2= round(($Vpa*$prod->Total)-$calculo, 2);//Esse é o lucro/prejuizo
                                        $arrayC[$nome] = $calculo2;
                                        $porcentagem= round(($Vpa*100)/($valor),2);

                                        if($calculo2<0){
                                            $arrayT[$nome] = -abs($porcentagem);
                                        }else{
                                            $arrayT[$nome] = $porcentagem;
                                        }
                                        
                                        $nome= "";
                                        $conf=false;
                                    }
                                    if($valor == $prod->atiSigla){
                                        $conf= true;
                                        $nome= $valor; // aqui pega o nome da acao
                                    }
                                }
                            }
                        }
                    }
                }

            }
            //dd($arrayC);

            $grafico = Produtos:: join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
            ->select( DB::raw('sum( produtos.proQuantidade ) as Total, at.atiSigla'))
            ->groupBy('produtos.ativos_atiCodigo')
            ->where('users_id', Auth::user()->id)
            ->get();

            if($grafico -> count()){
                foreach ($grafico as $gra) {
                    $array [$gra->atiSigla] = $gra->Total;    
            }


            return view('rendimentos.homeUser',compact('produtos', 'cotacao2', 'array', 'arrayC', 'arrayT','PA', 'PM'));


            
        }
            
        }
        if(Auth::user()->tipo==1){
            //Adm

                $usuarios= User :: select(DB::raw('COUNT( id ) as Total')) -> get();
                return view('index',compact('usuarios'));
        }
    }

    public function gerarPDF(SheetService $sheetService){

        $produtos= Produtos:: join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
            ->select( DB::raw('sum( produtos.proQuantidade ) as Total, sum( produtos.proPreco ) as Preco, count(produtos.proCodigo) as TotalId, at.atiSigla, at.atiNome'))
            ->groupBy('produtos.ativos_atiCodigo')
            ->where('users_id', Auth::user()->id)
            ->where('proOperacao', 1)
            ->get(); 

        $cotacao2 = $sheetService->get();

        $PM= Produtos:: join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
            ->select( DB::raw('produtos.proQuantidade, produtos.proPreco, at.atiSigla'))
            ->where('users_id', Auth::user()->id)
            ->get(); 

        $PA= [];

            foreach($PM as $prod){
                $quantidadeTotal = 0 ;
                $soma=0;
                $preco= 0;
                $pm=0;
                $final=0;
                foreach($PM as $p){
                    if($prod->atiSigla == $p->atiSigla){
                        $quantidadeTotal =  $soma + $p->proQuantidade;
                        $preco= $pm + ($p->proPreco *$p->proQuantidade);
                    }
                    $soma= $quantidadeTotal;
                    $pm= $preco;
                    if($pm>0){
                        $final= $pm/$soma;
                    }
                }
                if(array_search ($prod->atiSigla, $PA) == true){
                }else{
                    $PA[] = $prod->atiSigla;
                    $PA[] = $final;
                }
            }

        $data = date("d/m/Y");
        $data = explode("/", $data);
        list($dia, $mes, $ano) = $data;

        if($mes == 1){ $mes="janeiro"; }
        if($mes == 2){ $mes="fevereiro"; }
        if($mes == 3){ $mes="março"; }
        if($mes == 4){ $mes="abril"; }
        if($mes == 5){ $mes="maio"; }
        if($mes == 6){ $mes="junho"; }
        if($mes == 7){ $mes="julho"; }
        if($mes == 8){ $mes="agosto"; }
        if($mes == 9){ $mes="setembro"; }
        if($mes == 10){ $mes="outubro"; }
        if($mes == 11){ $mes="novembro"; }
        if($mes == 12){ $mes="dezembro"; }

        $data= "$dia de $mes de $ano";
        
        $pdf = PDF::loadView('rendimentos.pdf', compact('produtos', 'cotacao2', 'data', 'PA', 'PM')); //espera uma view

        return $pdf->setPaper('a4')->stream('rendimentos.pdf');
    }
}
