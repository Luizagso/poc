<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Estados;
use App\Enderecos;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

    public function __construct(){

        //$this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('usuario.conta');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estados = DB::table('estados')
         ->get();
        return view('auth.register')->with('estados', $estados);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo("show");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $estados = DB::table('estados') ->get();
        return view('usuario.update', compact(['user','estados']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->cpf = $request->cpf;
        $user->cel = $request->cel;
        $user->enderecos_endCodigo = $request->enderecos_endCodigo;

        if($request->senha==""){

        }else{
            $user->password = Hash::make($request->senha);
        }
        $user->update();


        $idEndereco = $user->enderecos_endCodigo;

        $enderecos = Enderecos::where('endCodigo',$idEndereco)->update([
            'endBairro' =>$request->endBairro,
            'endRua' =>$request->endRua,
            'endNumero' =>$request ->endNumero,
            'endComplemento' =>$request->endComplemento,
            'cidades_cidCodigo' =>$request->cidades_cidCodigo
        ]);

        $enderecos = Enderecos :: find($idEndereco);
        return view('usuario.conta', compact( 'enderecos' ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User:: find($id);
        $idEndereco = $user->enderecos_endCodigo;

        $enderecos = Enderecos::where('endCodigo',$idEndereco)->delete();
        $user->delete();
        
        return redirect()->route('login')
                        ->withSuccess('Conta apagada com sucesso!');
        
    }
}
