<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produtos;
use Auth;
use Illuminate\Support\Facades\DB;

class ImpostoController extends Controller
{
    public function index(){


        $com1= Produtos:: join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
        ->select( DB::raw('sum( produtos.proQuantidade ) as Total, sum( produtos.proPreco ) as Preco, count(produtos.proCodigo) as TotalId, at.atiSigla, at.atiNome'))
        ->groupBy('produtos.ativos_atiCodigo')
        ->whereYear('produtos.proData', '2018')
        ->where('users_id', Auth::user()->id)
        ->where('proOperacao', 1) // 1-compra
        ->get();

        $ven1= Produtos:: join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
        ->select( DB::raw('sum( produtos.proQuantidade ) as Total, sum( produtos.proPreco ) as Preco, count(produtos.proCodigo) as TotalId, at.atiSigla, at.atiNome'))
        ->groupBy('produtos.ativos_atiCodigo')
        ->whereYear('produtos.proData', '2018')
        ->where('users_id', Auth::user()->id)
        ->where('proOperacao', 2) // 2-venda
        ->get();

        $com2= Produtos:: join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
        ->select( DB::raw('sum( produtos.proQuantidade ) as Total, sum( produtos.proPreco ) as Preco, count(produtos.proCodigo) as TotalId, at.atiSigla, at.atiNome'))
        ->groupBy('produtos.ativos_atiCodigo')
        ->whereYear('produtos.proData', '2019')
        ->where('users_id', Auth::user()->id)
        ->where('proOperacao', 1) // 1-compra
        ->get();

        $ven2= Produtos:: join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
        ->select( DB::raw('sum( produtos.proQuantidade ) as Total, sum( produtos.proPreco ) as Preco, count(produtos.proCodigo) as TotalId, at.atiSigla, at.atiNome'))
        ->groupBy('produtos.ativos_atiCodigo')
        ->whereYear('produtos.proData', '2019')
        ->where('users_id', Auth::user()->id)
        ->where('proOperacao', 2) // 2-venda
        ->get();


        $com3= Produtos:: join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
        ->select( DB::raw('sum( produtos.proQuantidade ) as Total, sum( produtos.proPreco ) as Preco, count(produtos.proCodigo) as TotalId, at.atiSigla, at.atiNome'))
        ->groupBy('produtos.ativos_atiCodigo')
        ->whereYear('produtos.proData', '2020')
        ->where('users_id', Auth::user()->id)
        ->where('proOperacao', 1) // 1-compra
        ->get();

        $ven3= Produtos:: join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
        ->select( DB::raw('sum( produtos.proQuantidade ) as Total, sum( produtos.proPreco ) as Preco, count(produtos.proCodigo) as TotalId, at.atiSigla, at.atiNome'))
        ->groupBy('produtos.ativos_atiCodigo')
        ->whereYear('produtos.proData', '2020')
        ->where('users_id', Auth::user()->id)
        ->where('proOperacao', 2) // 2-venda
        ->get();
        ////

        $div1= Produtos:: join('dividendos as dividendos','dividendos.divCodigo','produtos.dividendos_divCodigo')
        ->join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
        ->select( DB::raw('sum( produtos.proQuantidade ) as Total, at.atiSigla, at.atiNome, dividendos.divValor, dividendos.divTipo'))
        ->groupBy('produtos.ativos_atiCodigo')
        ->whereYear('dividendos.divData', '2018')
        ->where('dividendos.divTipo',"Dividendo")
        ->where('users_id', Auth::user()->id)
        ->where('produtos.proOperacao',1)
        ->get();

        $div2= Produtos:: join('dividendos as dividendos','dividendos.divCodigo','produtos.dividendos_divCodigo')
        ->join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
        ->select( DB::raw('sum( produtos.proQuantidade ) as Total, at.atiSigla, at.atiNome, dividendos.divValor, dividendos.divTipo'))
        ->groupBy('produtos.ativos_atiCodigo')
        ->whereYear('dividendos.divData', '2019')
        ->where('dividendos.divTipo',"Dividendo")
        ->where('produtos.proOperacao',1)
        ->where('users_id', Auth::user()->id)
        ->get();
        
        $div3= Produtos:: join('dividendos as dividendos','dividendos.divCodigo','produtos.dividendos_divCodigo')
        ->join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
        ->select( DB::raw('sum( produtos.proQuantidade ) as Total, at.atiSigla, at.atiNome, dividendos.divValor, dividendos.divTipo'))
        ->groupBy('produtos.ativos_atiCodigo')
        ->whereYear('dividendos.divData', '2020')
        ->where('dividendos.divTipo',"Dividendo")
        ->where('produtos.proOperacao',1)
        ->where('users_id', Auth::user()->id)
        ->get();

        $jscp1= Produtos:: join('dividendos as dividendos','dividendos.divCodigo','produtos.dividendos_divCodigo')
        ->join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
        ->select( DB::raw('sum( produtos.proQuantidade ) as Total, at.atiSigla, at.atiNome, dividendos.divValor, dividendos.divTipo'))
        ->groupBy('produtos.ativos_atiCodigo')
        ->whereYear('dividendos.divData', '2018')
        ->where('dividendos.divTipo',"JSCP")
        ->where('produtos.proOperacao',1)
        ->where('users_id', Auth::user()->id)
        ->get();

        $jscp2= Produtos:: join('dividendos as dividendos','dividendos.divCodigo','produtos.dividendos_divCodigo')
        ->join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
        ->select( DB::raw('sum( produtos.proQuantidade ) as Total, at.atiSigla, at.atiNome, dividendos.divValor, dividendos.divTipo'))
        ->groupBy('produtos.ativos_atiCodigo')
        ->whereYear('dividendos.divData', '2019')
        ->where('dividendos.divTipo',"JSCP")
        ->where('produtos.proOperacao',1)
        ->where('users_id', Auth::user()->id)
        ->get();

        $jscp3= Produtos:: join('dividendos as dividendos','dividendos.divCodigo','produtos.dividendos_divCodigo')
        ->join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
        ->select( DB::raw('sum( produtos.proQuantidade ) as Total, at.atiSigla, at.atiNome, dividendos.divValor, dividendos.divTipo'))
        ->groupBy('produtos.ativos_atiCodigo')
        ->whereYear('dividendos.divData', '2020')
        ->where('dividendos.divTipo',"JSCP")
        ->where('produtos.proOperacao',1)
        ->where('users_id', Auth::user()->id)
        ->get();

        $PMC2= Produtos:: join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
            ->select( DB::raw('produtos.proQuantidade, produtos.proPreco, at.atiSigla'))
            ->where('users_id', Auth::user()->id)            
            ->whereYear('produtos.proData', '2019')
            ->where('proOperacao', 1) // 1-compra
            ->get(); 

            $PMV2= Produtos:: join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
            ->select( DB::raw('produtos.proQuantidade, produtos.proPreco, at.atiSigla'))
            ->where('users_id', Auth::user()->id)            
            ->whereYear('produtos.proData', '2019')
            ->where('proOperacao', 2) // 1-compra
            ->get(); 

       // dd($PMC2);

        return view('imposto.index',compact('com1', 'ven1', 'com2', 'ven2', 'com3', 'ven3', 'div1', 'div2', 'div3', 'jscp1','jscp2','jscp3', 'PMC2', 'PMV2'));
    }
}
