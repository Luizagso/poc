<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ativos;
use App\Http\Requests\AtivosRequest;

class AtivosController extends Controller
{

    private $objAtivos;

    public function __construct(){
       $this-> objAtivos = new Ativos;

        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ativos= $this->objAtivos->all();
        return view('ativos.index',compact('ativos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ativos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AtivosRequest $request)
    {
        $cadastro = $this->objAtivos->create([
            'atiNome' =>$request->atiNome,
            'atiSigla' =>$request->atiSigla,
            'atiCNPJ' =>$request->atiCNPJ,

        ]);
        if($cadastro){
            return redirect()->route('ativos.index')
            ->withSuccess('Ativo cadastrado!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ativos = $this->objAtivos->find($id);
        return view('ativos.update', compact('ativos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AtivosRequest $request, $id)
    {
        $this->objAtivos->where(['atiCodigo'=>$id])->update([
            'atiNome' =>$request->atiNome,
            'atiSigla' =>$request->atiSigla,
            'atiCNPJ' =>$request->atiCNPJ,
        ]);
        return redirect()->route('ativos.index')
        ->withSuccess('Ativo editado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ativos = Ativos::find($id);
        if ( $ativos == null) {
            return response()->json(false);
        }
        $ativos->delete();
        return response()->json(true);
    }

    public function dataTableJson(){
        return response()->json(['data'=>Ativos::all()]);
    }
}
