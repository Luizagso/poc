<?php

namespace App\Http\Controllers;
use App\Dividendos;
use App\Produtos;
use Illuminate\Http\Request;
use App\Http\Requests\DividendosRequest;
use Illuminate\Support\Facades\DB;
use Auth;
use PDF;

class DividendosController extends Controller
{

    private $objDividendos;

    public function __construct(){
       $this-> objDividendos = new Dividendos;

        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produtos= Produtos:: join('dividendos as dividendos','dividendos.divCodigo','produtos.dividendos_divCodigo')
            ->join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
            ->select( DB::raw('sum( produtos.proQuantidade ) as Total, at.atiSigla, at.atiNome, dividendos.divCodigo, dividendos.divValor, dividendos.divData, dividendos.divTipo'))
            ->groupBy('produtos.ativos_atiCodigo')
            ->where('produtos.proOperacao', 1)
            ->where('users_id', Auth::user()->id)
            ->get();
    
        return view('dividendos.index',compact('produtos'));
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $produtos = Produtos::where('users_id', Auth::user()->id)
        //->where('dividendos_divCodigo', NULL)
        ->groupBy('ativos_atiCodigo')
        ->where('produtos.proOperacao', 1)
        ->get();
        return view('dividendos.create', compact('produtos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DividendosRequest $request)
    {
        $result = new Dividendos;
        $result->divData = $request->divData;
        $result->divValor = ((float) $request->divValor);
        $result->divTipo = $request->divTipo;
        $result->save();

        $produtos = Produtos::where('ativos_atiCodigo',$request->ativos_atiCodigo)
            ->update(['dividendos_divCodigo' => $result->divCodigo]);
        
        return redirect()->route('dividendos.index')
            ->withSuccess('Cadastro realizado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dividendos= Dividendos:: find($id); //manda os dados dos dividendos OK

        $produtos = Produtos::where('users_id', Auth::user()->id)
        ->groupBy('ativos_atiCodigo')
        ->where('produtos.proOperacao', 1)
        ->get();// preenche o novo select OK

        $div = Produtos::where('dividendos_divCodigo', $id)
            ->join('ativos as ativos','produtos.ativos_atiCodigo','ativos.atiCodigo')
            ->where('produtos.users_id',Auth::user()->id)
            ->get(); //preenche com o ativo já selecionado

        return view('dividendos.update', compact('produtos', 'div', 'dividendos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DividendosRequest $request, $id)
    {
        //recebe cod dividendo
        $this->objDividendos->where(['divCodigo'=>$id])->update([
            'divData' => $request->divData,
            'divValor' =>((float) $request->divValor),
            'divTipo' =>$request->divTipo,
        ]);

        $produtos= Produtos:: where('dividendos_divCodigo', $id)
            ->update(['dividendos_divCodigo' => NULL]);

        $produtos2= Produtos::where('ativos_atiCodigo',$request->ativos_atiCodigo)
            ->update(['dividendos_divCodigo' => $id]);

        return redirect()->route('dividendos.index')
        ->withSuccess('Alteração realizada com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produtos= Produtos:: where('dividendos_divCodigo', $id)
            ->update(['dividendos_divCodigo' => NULL]);

        $dividendos = Dividendos::find($id);
        if ( $dividendos == null) {
            return response()->json(false);
        }
        $dividendos->delete();
        return response()->json(true);
    }

    public function gerarPDF(){

        $data = date("d/m/Y");
        $data = explode("/", $data);
        list($dia, $mes, $ano) = $data;

        if($mes == 1){ $mes="janeiro"; }
        if($mes == 2){ $mes="fevereiro"; }
        if($mes == 3){ $mes="março"; }
        if($mes == 4){ $mes="abril"; }
        if($mes == 5){ $mes="maio"; }
        if($mes == 6){ $mes="junho"; }
        if($mes == 7){ $mes="julho"; }
        if($mes == 8){ $mes="agosto"; }
        if($mes == 9){ $mes="setembro"; }
        if($mes == 10){ $mes="outubro"; }
        if($mes == 11){ $mes="novembro"; }
        if($mes == 12){ $mes="dezembro"; }

        $data= "$dia de $mes de $ano";

        $produtos= Produtos:: join('dividendos as dividendos','dividendos.divCodigo','produtos.dividendos_divCodigo')
            ->join('ativos as at','at.atiCodigo','produtos.ativos_atiCodigo')
            ->select( DB::raw('sum( produtos.proQuantidade ) as Total, at.atiSigla, at.atiNome, dividendos.divCodigo, dividendos.divValor, dividendos.divData, dividendos.divTipo'))
            ->groupBy('produtos.ativos_atiCodigo')
            ->where('produtos.proOperacao', 1)
            ->where('users_id', Auth::user()->id)
            ->get();

        $dividendos= Produtos:: join('dividendos as dividendos','dividendos.divCodigo','produtos.dividendos_divCodigo')
            ->select( DB::raw('sum( produtos.proQuantidade ) as Total, dividendos.divValor'))
            ->where('produtos.proOperacao', 1)
            ->where('users_id', Auth::user()->id)
            ->get();

        $Total= 0;
        $Calculo = 0;
    
        foreach($dividendos as $dividendos){
            $Soma= $Total;
            $Calculo= $dividendos->divValor * $dividendos->Total;
            $Total= $Soma + $Calculo ; 
        }

        $pdf = PDF::loadView('dividendos.pdf', compact('produtos', 'data', 'Total', )); //espera uma view

        return $pdf->setPaper('a4')->stream('dividendos.pdf');
    }
}
