<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DividendosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'divTipo' => 'required',
            'divValor' => 'required',
            'divData' => 'required',
            //'ativos_atiCodigo' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'divTipo.required' => 'É necessário selecionar o tipo!',
            'divValor.required' => 'O campo valor por ação é obrigatório!',
            'divData.required' => 'O campo data é obrigatório!',
            //'ativos_atiCodigo.required' => 'É necessário selecionar o ativo!',
        ];
    }
}
