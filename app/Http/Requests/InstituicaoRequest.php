<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InstituicaoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'insNome' => 'required',
            //'insDescricao'=> 'required'
        ];
    }

    public function messages()
    {
        return [
            'insNome.required' => 'O campo Nome é obrigatório!',
            //'insDescricao.required' => 'O campo Descrição é obrigatório!'
        ];
    }
}
