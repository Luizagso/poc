<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EnderecosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /*'endBairro' => 'required',
            'endRua'=> 'required',
            'endNumero'=> ['required', 'numeric'],
            'cidades_cidCodigo' => 'required'*/
        ];
    }

    public function messages()
    {
        return [
            /*'endBairro.required' => 'O campo Bairro é obrigatório!',
            'endRua.required' => 'O campo Rua é obrigatório!',
            'endNumero.required' => 'O campo Número é obrigatório!',
            'endNumero.numeric' => 'O campo Número requer um valor numerico!',
            'cidades_cidCodigo.required' => 'O campo Cidade é obrigatório!'*/
        ];
    }
}
