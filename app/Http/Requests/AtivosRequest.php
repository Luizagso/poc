<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AtivosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'atiNome' => 'required',
            'atiSigla'=> 'required',
            'atiCNPJ' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'atiNome.required' => 'O campo Nome é obrigatório!',
            'atiSigla.required' => 'O campo Sigla é obrigatório!',
            'atiCNPJ.required' => 'O campo CNPJ é obrigatório!',
        ];
    }
}
