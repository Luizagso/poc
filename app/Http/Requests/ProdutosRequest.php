<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProdutosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'instituicao_insCodigo' => 'required',
            'ativos_atiCodigo'=> 'required',
            'proData' => 'required',
            'proPreco' => 'required',
            'proQuantidade' => 'required',
            'proOperacao' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'instituicao_insCodigo.required' => 'É necessário selecionar a Instituição Financeira!',
            'ativos_atiCodigo.required' => 'É necessário selecionar o Ativo!',
            'proData.required' => 'O campo data de compra é obrigatório!',
            'proPreco.required' => 'O campo preço de compra é obrigatório!',
            'proQuantidade.required' => 'O campo quantidade é obrigatório!',
            'proOperacao.required' => 'É necessário selecionar a operação!',
        ];
    }
}
