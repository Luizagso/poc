<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ativos extends Model
{
    protected $table= 'ativos';
    protected $primaryKey= 'atiCodigo';
    protected $fillable= ['atiNome', 'atiSigla','atiCNPJ'];
}
