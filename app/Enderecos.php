<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enderecos extends Model
{
    protected $table= 'enderecos';
    protected $primaryKey= 'endCodigo';
    protected $fillable= ['endBairro', 'endRua', 'endNumero', 'endComplemento', 'cidades_cidCodigo'];

    public function cidades(){
        return $this->belongsTo('App\Cidades', 'cidades_cidCodigo', 'cidCodigo');
    }
}
