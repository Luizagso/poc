<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Produtos extends Model
{
    protected $table= 'produtos';
    protected $primaryKey= 'proCodigo';
    protected $fillable= ['proData', 'proQuantidade','proPreco','proTaxa','proOperacao','ativos_atiCodigo','users_id','instituicao_insCodigo', 'dividendos_divCodigo'];

    public function ativos(){
        return $this->belongsTo('App\Ativos', 'ativos_atiCodigo', 'atiCodigo');
    }

    public function user(){
        return $this->belongsTo('App\User', 'users_id', 'id');
    }

    public function instituicao(){
        return $this->belongsTo('App\Instituicao', 'instituicao_insCodigo', 'insCodigo');
    }
    public function dividendos(){
        return $this->belongsTo('App\Dividendos', 'dividendos_divCodigo', 'divCodigo');
    }
}
