<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Estados;

class Cidades extends Model
{
    protected $table= 'cidades';
    protected $primaryKey= 'cidCodigo';

        public function estados(){
            return $this->belongsTo('App\Estados', 'estados_estCodigo', 'estCodigo');
        }

}
