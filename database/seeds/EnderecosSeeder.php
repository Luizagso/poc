<?php

use Illuminate\Database\Seeder;
use App\Enderecos;

class EnderecosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Enderecos::create([
        	'endCodigo'	=>	1,
        	'endBairro'	=>	'Areis Brancas',
            'endRua'	=>	'Rua Severo Júlio da Silveira',
            'endNumero' => 25,
        	'endComplemento'=>	'A',
            'cidades_cidCodigo'=> 1,
        ]);
        Enderecos::create([
        	'endCodigo'	=>	2,
        	'endBairro'	=>	'Areis Brancas',
            'endRua'	=>	'Rua Severo Júlio da Silveira',
            'endNumero' => 25,
        	'endComplemento'=>	'A',
            'cidades_cidCodigo'=> 1,
        ]);
    }
}
