<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AtivosSeeder::class);
        $this->call(InstituicaoSeeder::class);
        $this->call(EnderecosSeeder::class);
        $this->call(UserSeeder::class);
    }
}
