<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Administrador
        User::create([
        	'id'		=>	1,
        	'name'		=>	'Luiza Garcia',
        	'email'		=>	'luizaoliveiragso@gmail.com',
        	'password'	=>	Hash::make('12345678'),
        	'cpf'	=>	'082.127.666-24',
        	'cel'	=>	'(37) 99152-6121',
        	'tipo'	=>	1,
            'enderecos_endCodigo'=> 1,
        ]);

        //Usuário
        User::create([
        	'id'		=>	2,
        	'name'		=>	'Ana',
        	'email'		=>	'ana@gmail.com',
        	'password'	=>	Hash::make('12345678'),
        	'cpf'	=>	'000.000.000-00',
        	'cel'	=>	'(00) 00000-0000',
        	'tipo'	=>	2,
            'enderecos_endCodigo'=> 2,
        ]);
    }
}
