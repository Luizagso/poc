<?php

use Illuminate\Database\Seeder;
use App\Instituicao;

class InstituicaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Instituicao::create([
            'insCodigo'    =>  1,
            'insNome'      =>  'Ativa',
            'insDescricao' => '',
        ]);
        Instituicao::create([
            'insCodigo'    =>  2,
            'insNome'      =>  'Banco do Brasil',
            'insDescricao' => '',
        ]);
        Instituicao::create([
            'insCodigo'    =>  3,
            'insNome'      =>  'Bradesco',
            'insDescricao' => '',
        ]);
        Instituicao::create([
            'insCodigo'    =>  4,
            'insNome'      =>  'Clear Corretora',
            'insDescricao' => '',
        ]);
        Instituicao::create([
            'insCodigo'    =>  5,
            'insNome'      =>  'Easynvest',
            'insDescricao' => '',
        ]);
        Instituicao::create([
            'insCodigo'    =>  6,
            'insNome'      =>  'Elite Corretora',
            'insDescricao' => '',
        ]);
        Instituicao::create([
            'insCodigo'    =>  7,
            'insNome'      =>  'Genial Investimentos',
            'insDescricao' => '',
        ]);
        Instituicao::create([
            'insCodigo'    =>  8,
            'insNome'      =>  'Itaú',
            'insDescricao' => '',
        ]);
        Instituicao::create([
            'insCodigo'    =>  9,
            'insNome'      =>  'Mirae',
            'insDescricao' => '',
        ]);
        Instituicao::create([
            'insCodigo'    =>  10,
            'insNome'      =>  'Modalmais ',
            'insDescricao' => '',
        ]);
        Instituicao::create([
            'insCodigo'    =>  11,
            'insNome'      =>  'Mycap',
            'insDescricao' => '',
        ]);
        Instituicao::create([
            'insCodigo'    =>  12,
            'insNome'      =>  'Nova Futura',
            'insDescricao' => '',
        ]);
        Instituicao::create([
            'insCodigo'    =>  13,
            'insNome'      =>  'Rico Corretora',
            'insDescricao' => '',
        ]);
        Instituicao::create([
            'insCodigo'    =>  14,
            'insNome'      =>  'Santander',
            'insDescricao' => '',
        ]);
        Instituicao::create([
            'insCodigo'    =>  15,
            'insNome'      =>  'Terra Investimentos',
            'insDescricao' => '',
        ]);
        Instituicao::create([
            'insCodigo'    =>  16,
            'insNome'      =>  'Toro Investimentos',
            'insDescricao' => '',
        ]);
        Instituicao::create([
            'insCodigo'    =>  17,
            'insNome'      =>  'XP Investimentos',
            'insDescricao' => '',
        ]);

    }
}
