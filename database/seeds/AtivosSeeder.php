<?php

use Illuminate\Database\Seeder;
use App\Ativos;

class AtivosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ativos::create([
            'atiCodigo'=>  1,
            'atiNome'  =>  'Vale',
            'atiSigla' => 'VALE3',
            'atiCNPJ'  => '33.592.510/0001-54',
        ]);
        Ativos::create([
            'atiCodigo'=>  2,
            'atiNome'  =>  'Itaú Unibanco',
            'atiSigla' => 'ITUB4',
            'atiCNPJ'  => '60.872.504/0001-23',
        ]);
        Ativos::create([
            'atiCodigo'=>  3,
            'atiNome'  =>  'Itaú Unibanco',
            'atiSigla' => 'ITUB3',
            'atiCNPJ'  => '60.872.504/0001-23',
        ]);
        Ativos::create([
            'atiCodigo'=>  4,
            'atiNome'  =>  'Itaú Unibanco',
            'atiSigla' => 'ITUB3F',
            'atiCNPJ'  => '60.872.504/0001-23',
        ]);
        Ativos::create([
            'atiCodigo'=>  5,
            'atiNome'  =>  'Itaú Unibanco',
            'atiSigla' => 'ITUB4F',
            'atiCNPJ'  => '60.872.504/0001-23',
        ]);
        Ativos::create([
            'atiCodigo'=>  6,
            'atiNome'  =>  'Bradesco',
            'atiSigla' => 'BBDC4',
            'atiCNPJ'  => '60.746.948/0001-12',
        ]);
        Ativos::create([
            'atiCodigo'=>  7,
            'atiNome'  =>  'AmBev',
            'atiSigla' => 'ABEV3',
            'atiCNPJ'  => '07.526.557/0001-00',
        ]);
        Ativos::create([
            'atiCodigo'=>  8,
            'atiNome'  =>  'Petrobras',
            'atiSigla' => 'PETR4',
            'atiCNPJ'  => '33.000.167/1049-00',
        ]);
        Ativos::create([
            'atiCodigo'=>  9,
            'atiNome'  =>  'MDiasBranco',
            'atiSigla' => 'MDIA3',
            'atiCNPJ'  => '07.973.152/0001-10',
        ]);
        Ativos::create([
            'atiCodigo'=>  10,
            'atiNome'  =>  'Fleury',
            'atiSigla' => 'FLRY3',
            'atiCNPJ'  => '60.840.055/0001-31',
        ]);
        Ativos::create([
            'atiCodigo'=>  11,
            'atiNome'  =>  'ViaVarejo',
            'atiSigla' => 'VVAR3',
            'atiCNPJ'  => '33.041.260/0652-90',
        ]);
        Ativos::create([
            'atiCodigo'=>  12,
            'atiNome'  =>  'Banco do Brasil',
            'atiSigla' => 'BBAS3',
            'atiCNPJ'  => '00.000.000/0001-91',
        ]);
        Ativos::create([
            'atiCodigo'=>  13,
            'atiNome'  =>  'Magazine Luiza',
            'atiSigla' => 'MGLU3',
            'atiCNPJ'  => '47.960.950/0001-21.',
        ]);
        Ativos::create([
            'atiCodigo'=>  14,
            'atiNome'  =>  'Magazine Luiza',
            'atiSigla' => 'MGLU3F',
            'atiCNPJ'  => '47.960.950/0001-21.',
        ]);
        Ativos::create([
            'atiCodigo'=>  15,
            'atiNome'  =>  'Itaúsa',
            'atiSigla' => 'ITSA4',
            'atiCNPJ'  => '61.532.644/0001-15',
        ]);
        Ativos::create([
            'atiCodigo'=>  16,
            'atiNome'  =>  'Itaúsa',
            'atiSigla' => 'ITSA4F',
            'atiCNPJ'  => '61.532.644/0001-15',
        ]);
        Ativos::create([
            'atiCodigo'=>  17,
            'atiNome'  =>  'Itaúsa',
            'atiSigla' => 'ITSA3F',
            'atiCNPJ'  => '61.532.644/0001-15',
        ]);
        Ativos::create([
            'atiCodigo'=>  18,
            'atiNome'  =>  'JBS',
            'atiSigla' => 'JBSS3',
            'atiCNPJ'  => '02.916.265/0001-60',
        ]);
        Ativos::create([
            'atiCodigo'=>  19,
            'atiNome'  =>  'Santander',
            'atiSigla' => 'SANB11',
            'atiCNPJ'  => '90.400.888/0001-42',
        ]);
        Ativos::create([
            'atiCodigo'=>  20,
            'atiNome'  =>  'Suzano',
            'atiSigla' => 'SUZB3',
            'atiCNPJ'  => '16.404.287/0001-55',
        ]);
        Ativos::create([
            'atiCodigo'=>  21,
            'atiNome'  =>  'Azul',
            'atiSigla' => 'AZUL4',
            'atiCNPJ'  => '09.305.994/0001-29',
        ]);
        Ativos::create([
            'atiCodigo'=>  22,
            'atiNome'  =>  'Amazon',
            'atiSigla' => 'AMZO34F',
            'atiCNPJ'  => '15.436.940/0001-03',
        ]);
        Ativos::create([
            'atiCodigo'=>  23,
            'atiNome'  =>  'Metal Leve',
            'atiSigla' => 'LEVE3F',
            'atiCNPJ'  => '60.476.884/0001-87',
        ]);
        Ativos::create([
            'atiCodigo'=>  24,
            'atiNome'  =>  'Metal Leve',
            'atiSigla' => 'LEVE3',
            'atiCNPJ'  => '60.476.884/0001-87',
        ]);
        Ativos::create([
            'atiCodigo'=>  25,
            'atiNome'  =>  'Movida',
            'atiSigla' => 'MOVI3F',
            'atiCNPJ'  => '21.314.559/0001-66',
        ]);
        Ativos::create([
            'atiCodigo'=>  26,
            'atiNome'  =>  'Movida',
            'atiSigla' => 'MOVI3',
            'atiCNPJ'  => '21.314.559/0001-66',
        ]);
        Ativos::create([
            'atiCodigo'=>  27,
            'atiNome'  =>  'CVC',
            'atiSigla' => 'CVCB3',
            'atiCNPJ'  => '10.760.260/0001-19',
        ]);
        Ativos::create([
            'atiCodigo'=>  28,
            'atiNome'  =>  'Renner',
            'atiSigla' => 'LREN3',
            'atiCNPJ'  => '92.754.738/0001-62',
        ]);
        Ativos::create([
            'atiCodigo'=>  29,
            'atiNome'  =>  'Rumo SA',
            'atiSigla' => 'RAIL3',
            'atiCNPJ'  => '02.387.241/0001-60',
        ]);
        Ativos::create([
            'atiCodigo'=>  30,
            'atiNome'  =>  'Cogna',
            'atiSigla' => 'COGN3',
            'atiCNPJ'  => '02.800.026/0001-40',
        ]);
        Ativos::create([
            'atiCodigo'=>  31,
            'atiNome'  =>  'Cielo',
            'atiSigla' => 'CIEL3',
            'atiCNPJ'  => '01.027.058/0001-91',
        ]);
        Ativos::create([
            'atiCodigo'=>  32,
            'atiNome'  =>  'Usiminas',
            'atiSigla' => 'USIM5',
            'atiCNPJ'  => '60.894.730/0001-05',
        ]);
        Ativos::create([
            'atiCodigo'=>  33,
            'atiNome'  =>  'Localiza',
            'atiSigla' => 'RENT3',
            'atiCNPJ'  => '16.670.085/0001-55',
        ]);
        Ativos::create([
            'atiCodigo'=>  34,
            'atiNome'  =>  'Microsoft',
            'atiSigla' => 'MSFT34F',
            'atiCNPJ'  => '03.082.929/0001-03',
        ]);
        Ativos::create([
            'atiCodigo'=>  35,
            'atiNome'  =>  'Microsoft',
            'atiSigla' => 'MSFT34',
            'atiCNPJ'  => '03.082.929/0001-03',
        ]);
        Ativos::create([
            'atiCodigo'=>  36,
            'atiNome'  =>  'Intel',
            'atiSigla' => 'ITLC34F',
            'atiCNPJ'  => '00.000.000/0000-00',
        ]);
        Ativos::create([
            'atiCodigo'=>  37,
            'atiNome'  =>  'Intel',
            'atiSigla' => 'ITLC34',
            'atiCNPJ'  => '00.000.000/0000-00',
        ]);
        Ativos::create([
            'atiCodigo'=>  38,
            'atiNome'  =>  'Natura',
            'atiSigla' => 'NTCO3',
            'atiCNPJ'  => '32.785.497/0001-97',
        ]);
        Ativos::create([
            'atiCodigo'=>  39,
            'atiNome'  =>  'Gol',
            'atiSigla' => 'GOLL4',
            'atiCNPJ'  => '06.164.253/0001-87',
        ]);
        Ativos::create([
            'atiCodigo'=>  40,
            'atiNome'  =>  'Embraer',
            'atiSigla' => 'EMBR3',
            'atiCNPJ'  => '07.689.002/0001-89',
        ]);

        
    }
}
