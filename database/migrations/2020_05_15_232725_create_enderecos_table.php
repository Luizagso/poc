<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnderecosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enderecos', function (Blueprint $table) {
            $table->bigIncrements('endCodigo');
            $table->string('endBairro')->nullable()->default(NULL);
            $table->string('endRua')->nullable()->default(NULL);
            $table->integer('endNumero')->nullable()->default(NULL);
            $table->string('endComplemento')->nullable()->default(NULL);
            $table->unsignedBigInteger('cidades_cidCodigo')->nullable()->default(NULL);
            $table->foreign('cidades_cidCodigo')->references('cidCodigo')->on('cidades')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enderecos');
    }
}
