<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->bigIncrements('proCodigo');
            $table->date('proData');
            $table->integer('proQuantidade');
            $table->float('proPreco');
            $table->float('proTaxa')->nullable()->default(NULL);
            $table->string('proOperacao');
            $table->unsignedBigInteger('ativos_atiCodigo');
            $table->foreign('ativos_atiCodigo')->references('atiCodigo')->on('ativos')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('users_id');
            $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('instituicao_insCodigo');
            $table->foreign('instituicao_insCodigo')->references('insCodigo')->on('instituicao')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('dividendos_divCodigo')->nullable()->default(NULL);
            $table->foreign('dividendos_divCodigo')->references('divCodigo')->on('dividendos')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
